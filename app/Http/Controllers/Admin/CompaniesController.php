<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CompaniesModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompaniesController extends Controller
{
    protected $companies;

    public function __construct(CompaniesModel $companies)
    {
        $this->companies = $companies;
    }

    public function index()
    {
        $companies = CompaniesModel::all();
        return response()->json(['companies' => $companies]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'company_code'   => 'required|unique:companies',
            'name'           => 'required',
            'location'       => 'required',
            'contact_person' => 'required'
        ]);

        $params = $request->all();

        $company = [
            'company_code'   => $params['company_code'],
            'name'           => $params['name'],
            'location'        => $params['location'],
            'contact_person' => $params['contact_person']
        ];

        $this->companies->fill($company);
        $this->companies->save();
        return Response(array('status' => 'success'), 200);
    }

    public function show($id)
    {
        $company = CompaniesModel::find($id);

        return response()->json(compact('company'));
    }

    public function update(Request $request, $id)
    {
        try {

            $params = $request->all();
            $company = CompaniesModel::findOrFail($id);
            $company->company_code   = $params['company_code'];
            $company->name           = $params['name'];
            $company->location       = $params['location'];
            $company->contact_person = $params['contact_person'];
            $company->save();

            return Response(array('status' => 'success'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {
        try {
            $company = CompaniesModel::findOrFail($id);
            $company->delete();

            return Response(array('status' => 'success'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);
        }
    }
}
