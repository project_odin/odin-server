<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;

use App\Models\DatabaseLogsModel;
use App\Models\AuditTrailModel;

class AdminController extends Controller
{
    public function index()
    {}

    public function create()
    {}

    public function store(Request $request)
    {}

    public function show($id)
    {}

    public function edit($id)
    {}

    public function update(Request $request, $id)
    {}

    public function destroy($id)
    {}

    public function backUpDatabase($userId, $type)
    {      
        $dbModel = new DatabaseLogsModel();
        $dbModel->saveDbLog($userId, $type);

    }

    public function getDbLogs()
    {   
        $dbModel = new DatabaseLogsModel();
        $logs = $dbModel->getLogs();
        return response()->json(['dblogs' =>$logs]);
    }

    public function getAuditTrail()
    {
        $audit  = new AuditTrailModel();
        $logs = $audit->fetchAll();
        return response()->json(['audit' => $logs]);
    }
}
