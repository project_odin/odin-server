<?php

namespace App\Http\Controllers\Admin;

use Hash;
use JWTAuth;
use App\Http\Requests;
use App\Models\Profile;
use App\Models\UserModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AuditTrailModel as Audit;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    protected $user;
    protected $profile;

    public function __construct(UserModel $user, Profile $profile)
    {
        $this->user = $user;
        $this->profile = $profile;
    }

    public function index()
    {
        $user = $this->user->fetchAll();
        return response()->json(['users' => $user]);

    }

    public function store(Request $request)
    {
        $logger = JWTAuth::parseToken()->authenticate();

        $data  = $request->all();

        $loginData = array (
            'username'  => $data['username'],
            'password'  => Hash::make($data['password']),
            'client_id' => $data['user_type'],
            'is_deleted' => 'n'
        );

        $this->user->fill($loginData);
        $this->user->save();

        $userId = $this->user->id;

        $profileDetails = array(
            'user_id'     => $userId,
            'first_name'  => $data['first_name'],
            'middle_name' => $data['middle_name'], 
            'last_name'   => $data['last_name'], 
        );

        $this->profile->fill($profileDetails);
        $this->profile->save();

        Audit::saveAudit($logger['attributes']['id'], "Created a user (User ID : $userId)");

        return Response(array('status' => 'success'), 200);
    }

    public function show($id)
    {
        $user = $this->user->getUserData($id);
        return response()->json(compact('user'));
    }

    public function update(Request $request, $id)
    {
        try {

            $params = $request->all();
            $user = UserModel::findOrFail($id);
            $user->username  = $params['username'];
            $user->password  = $params['password'];
            $user->client_id = $params['client_id'];
            $user->save();

            return Response(array('status' => 'success'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {

        $user = UserModel::find($id);
        $user->delete($id);

        $logger = JWTAuth::parseToken()->authenticate();
        Audit::saveAudit($logger['attributes']['id'], "Deleted a user (User ID : $id)");
    }
}
