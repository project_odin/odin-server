<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\PositionsModel;    

class PositionsController extends Controller
{   
    protected $positions;

    public function __construct(PositionsModel $positions)
    {
        $this->positions = $positions;
    }

    public function index()
    {
        $positions = PositionsModel::all();
        return response()->json(['positions' => $positions]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'position'    => 'required|unique:positions',
            'description' => 'required',
        ]);

        $params = $request->all();

        $position = [
            'position'    => $params['position'],
            'description' => $params['description']
        ];

        $this->positions->fill($position);
        $this->positions->save();

        return Response(array('status' => 'success'), 200);
    }

    public function show($id)
    {
        $position = PositionsModel::find($id);

        return response()->json(compact('position'));
    }

    public function update(Request $request, $id)
    {
        try {

            $params = $request->all();
            $company = PositionsModel::findOrFail($id);
            $company->position    = $params['position'];
            $company->description = $params['description'];
            $company->save();

            return Response(array('status' => 'success'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {
        try {
            $company = PositionsModel::findOrFail($id);
            $company->delete();

            return Response(array('status' => 'success'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);
        }
    }
}
