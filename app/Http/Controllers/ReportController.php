<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use App\Http\Controllers\Controller;

use App\Models\OpeningModel;
use App\Models\AuditTrailModel;
use App\Models\ApplicantModel;
use App\Models\ResumeModel;
use App\Models\OpeningCandidatesModel;
use App\Models\PayrollModel;
use App\Models\EmployeeModel;

use PDF;

class ReportController extends Controller
{
    protected $opening; 
    protected $audit; 
    protected $applicant;
    protected $resume;
    protected $openingCandidates;
    protected $payroll;
    protected $employee;
    
    public function __construct(
        OpeningModel           $opening,
        AuditTrailModel        $audit,
        ApplicantModel         $applicant,
        ResumeModel            $resume,
        OpeningCandidatesModel $openingCandidates,
        PayrollModel           $payroll,
        EmployeeModel          $employee
    ) {
        $this->opening           = $opening;
        $this->audit             = $audit;
        $this->applicant         = $applicant;
        $this->resume            = $resume;
        $this->openingCandidates = $openingCandidates;
        $this->payroll           = $payroll;
        $this->employee          = $employee;
    }

    function generateReport(Request $request) 
    {
    
        $params = $request->all();

        $reportType = $params['report_type'];

        switch ($reportType) {
            case 'employee'     : 
                return $this->generateEmployeeReport($params); 
                break;
            case 'resignation'  : 
                return $this->generateResignationsReport($params); 
                break;
            case 'termination'  : 
                return $this->generateTerminationsReport($params); 
                break;
            case 'leave'        : 
                return $this->generateLeaveReport($params); 
                break;
            case 'opening'      : 
                return $this->generateOpeningReport($params); 
                break;
            case 'candidate'    : 
                return $this->generateCandidateReport($params); 
                break;
            case 'payroll'      : 
                return $this->generatePayrollReport($params); 
                break;
            case 'payslip' : 
                return $this->generatePayslipReport($params); 
                break;
            case 'announcement' : 
                return $this->generateAnnouncementReport($params); 
                break;
            case 'audit' : 
                return $this->generateAuditReport($params); 
                break;
            case 'resume' : 
                return $this->displayCandidateResume($params); 
                break;
        }

        // if ($reportType === 'openings') {
        // } else if ($reportType === 'audit') {
        //     $data = array('data' => $this->audit->fetchAll());
        //     $pdf = PDF::loadView('pdf.audit', $data)->setOrientation('landscape');
        //     $filename = 'report_audit' . date('Y-m-d') . '.pdf';
        //     return $pdf->stream($filename);
        // } else if ($reportType === 'resume') {

        //     if ($id !== '0') {
        //         $resume = $this->resume->fetch($id, true);
        //         $path = 'public/candidates/resume/' . $resume[0]->file;
        //         return Response::make(file_get_contents($path), 200, [
        //             'Content-Type' => 'application/pdf',
        //             'Content-Disposition' => 'inline;',
        //         ]);

        //     } else {

        //         $data = array('data' => $this->openingCandidates->fetchAll());
        //         $pdf = PDF::loadView('pdf.resumes', $data)->setOrientation('landscape');
        //         $filename = 'report_audit' . date('Y-m-d') . '.pdf';
        //         return $pdf->stream($filename);
        //     }
        // } else if ($reportType === 'payslip') {

        //         $data = array('data' => $this->payroll->getPayrollDetails($id));
        //         $pdf = PDF::loadView('pdf.payslip', $data)->setOrientation('portrait');
        //         $filename = 'payslip' . date('Y-m-d') . '.pdf';

        //         return $pdf->stream($filename);
        // } else if ($reportType === 'payroll') {
        //         $data = array('data' => $this->payroll->getPayrollSummary());
        //         $pdf = PDF::loadView('pdf.payroll', $data)->setOrientation('landscape');
        //         $filename = 'payroll_summary' . date('Y-m-d') . '.pdf';
                
        //         return $pdf->stream($filename);
        // }

    }

    public function generateEmployeeReport($params)
    {
        $data = array('data' => $this->employee->getEmployees($params));
        $pdf = PDF::loadView('pdf.employees', $data);
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename); 
    }

    public function generatePayslipReport($params)
    {
        $data = array('data' => $this->payroll->getPayrollDetails($params['emp_no']));
        $pdf = PDF::loadView('pdf.payslip', $data)->setOrientation('portrait');
        $filename = 'payslip' . date('Y-m-d') . '.pdf';

        return $pdf->stream($filename);
    }

    public function generateResignationsReport($params)
    {
        $pdf = PDF::loadView('pdf.resignation');
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename); 
    }

    public function generateTerminationsReport($params)
    {
        $pdf = PDF::loadView('pdf.terminations');
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename); 
    }

    public function generateLeaveReport($params)
    {
        $pdf = PDF::loadView('pdf.leaves');
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename); 
    }

    public function generateOpeningReport($params)
    {
        $data = array('data' => $this->opening->getOpenings());
        $pdf = PDF::loadView('pdf.invoice', $data);
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename);
    }

    public function generateCandidateReport($params)
    {
        $data = array('data' => $this->openingCandidates->fetchAll());
        $pdf = PDF::loadView('pdf.resumes', $data)->setOrientation('landscape');
        $filename = 'report_audit' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename);
    }

    public function generatePayrollReport($params)
    {
        $data = array('data' => $this->payroll->getPayrollSummary($params));
        $pdf = PDF::loadView('pdf.payroll', $data)->setOrientation('landscape');
        $filename = 'payroll_summary' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename);
    }

    public function generateAnnouncementReport($params)
    {
        $pdf = PDF::loadView('pdf.announcement');
        $filename = 'report_candidates' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename); 
    }

    public function generateAuditReport($params)
    {
        $data = array('data' => $this->audit->fetchAll());
        $pdf = PDF::loadView('pdf.audit', $data)->setOrientation('landscape');
        $filename = 'report_audit' . date('Y-m-d') . '.pdf';
        return $pdf->stream($filename);
    }

    public function displayCandidateResume($params)
    {
        $resume = $this->resume->fetch($params['candidate_id'], true);
        $path = 'public/candidates/resume/' . $resume[0]->file;
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline;',
        ]);
    }
}
