<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\ResignationsModel;

class ResignationsController extends Controller
{

    public function __construct(ResignationsModel $resignation)
    {
        $this->resignation = $resignation;
    }

    public function index()
    {      
        $resignations = $this->resignation->fetchAll();         
        return response()->json(['resignations' => $resignations]);
    }

    public function store(Request $request)
    {   

        $this->validate($request, [
            'employee_number'  => 'required',
            'date_notice'      => 'required',
            'resignation_date' => 'required',
            'reason'           => 'required',
            'note'             => 'required'
        ]);

        $params = $request->all();
        $this->resignation->saveResignation($params);
        return Response(array('status' => 'success', 'message' => 'Resignation saved.'), 200);

    }

    public function show($id)
    {

        try {
            $resignation = $this->resignation->fetch($id);
            return response()->json(['resignation' => $resignation]);
        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }

    }

    public function update(Request $request, $id)
    {
        try {
            $params = $request->all();
            $this->resignation->updateResignation($params, $id);
            return Response(array('status' => 'success', 'message' => 'Resignation updated.'), 200);

        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }
    }

    public function destroy($id)
    {
        try {
            $this->resignation->deleteResignation($id);
            return Response(array('status' => 'success', 'message' => 'Resignation deleted.'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }

    }
}
