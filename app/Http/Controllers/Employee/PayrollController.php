<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PayrollModel;
use JWTAuth;
class PayrollController extends Controller
{   

    protected $payroll;

    public function __construct(PayrollModel $payroll)
    {
        $this->payroll = $payroll;
    }

    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->username;
        
        $payroll = $this->payroll->getPayrollDetails($userId);

        return response()->json(['payroll' => $payroll]);

    }

    public function create()
    {}

    public function store(Request $request)
    {}

    public function show($id)
    {}

    public function edit($id)
    {}

    public function update(Request $request, $id)
    {}

    public function destroy($id)
    {}
}
