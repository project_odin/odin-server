<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\EmployeeModel;
use App\User;

class EmployeeController extends Controller
{

    protected $employee;

    public function __construct(EmployeeModel $employee )
    {
        $this->employee = $employee; 
    }

    public function index()
    {}

    public function store(Request $request)
    {}

    public function show($id)
    {
        $employee = User::with(
            'employees', 
            'profiles', 
            'employees.positions', 
            'employees.departments',
            'employees.exemptions'
        )->find($id);
        return response()->json(['employee' => $employee]); 
    }

    public function update(Request $request, $id)
    {

        try {

            $params = $request->all();

            $employee = $this->employee->find($id);

            $employee->contact_number     = $params['contact_number'];
            $employee->civil_status       = $params['civil_status'];
            $employee->gender             = $params['gender'];
            $employee->address            = $params['address'];
            $employee->birth_date         = $params['birth_date'];
            $employee->save();

            return Response(array('status' => 'success', 'message' => 'Update profile success.'), 200);

        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }
    }

    public function destroy($id)
    {}
}
