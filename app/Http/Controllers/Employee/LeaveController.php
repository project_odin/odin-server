<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LeaveInformationModel;
use App\Models\LeaveRequestsModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LeaveController extends Controller
{
    protected $leaveInformation;    
    protected $leaveRequests;

    public function __construct(LeaveInformationModel $leaveInformation, LeaveRequestsModel $leaveRequests)
    {
        $this->leaveInformation = $leaveInformation;
        $this->leaveRequests = $leaveRequests;
    }

    public function index()
    {}

    public function getleaveRequests($user)
    {
        $leaves = $this->leaveRequests->getLeaveRequest($user);
        return response()->json(['leaves' => $leaves]);
    }

    public function store(Request $request)
    {
        $params = $request->all();
        
        $this->validate($request, [
            'leave_type_id'    => 'required',
            'date_start'       => 'required',
            'date_end'         => 'required',
            'reason'           => 'required',
            'employee_remarks' => 'required'
        ]);

        $leave = [
            'leave_type_id'    => $params['leave_type_id'],
            'user_id'          => $params['user_id'],
            'date_start'       => $params['date_start'], 
            'date_end'         => $params['date_end'], 
            'reason'           => $params['reason'],
            'employee_remarks' => $params['employee_remarks'],
            'leave_status'     => 'p'
        ];
                
        $this->leaveRequests->fill($leave);
        $this->leaveRequests->save();
        
        return Response(array('status' => 'success'), 200);
    }

    public function show($id)
    {
        $leave = LeaveRequestsModel::with('leave_information')->find($id);
        return response()->json(compact('leave'));
    }

    public function update(Request $request, $id)
    {
        try {
            $params = $request->all();
            $this->leaveRequests->updateLeaveRequest($params, $id);
            return Response(array('status' => 'success', 'message' => 'Leave Request updated.'), 200);

        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }
    }

    public function destroy($id)
    {
        //
    }
}
