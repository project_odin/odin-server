<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App\Models\UserModel;
use App\Models\EmployeeModel;
use Hash;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserModel $user, EmployeeModel $employee)
    {
        $this->user = $user;
        $this->employee = $employee;
    }

    public function index()
    {}

    public function create()
    {}

    public function store(Request $request)
    {}

    public function show($id)
    {
        $user = $this->user->find($id);

        return $this->user->getUserDetails($user);

    }

    public function edit($id)
    {}

    public function update(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'username' => "required|unique:users,username,{$id}",
                'password' => 'required|min:8'
            ]);

            $params = $request->all();
            $user = UserModel::findOrFail($id);
            $user->username = $params['username'];
            $user->password = Hash::make($params['password']);
            $user->save();

            return Response(array('status' => 'success', 'message' => 'Update credentials success.'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {}

    public function resetPassword($username)
    {

        $user = $this->user->searchUser($username);

        if ($user[0]->client_id === 'applicant') {
            $email = $user[0]->username;
            $user   = $this->user->searchUser($username);
            $userId = $user[0]->id;

        } else if ($user[0]->client_id === 'employee'){
            $user   = $this->employee->getEmployee($username);
            $email  = $user[0]->email;
            $userId = $user[0]->user_id;
        }

        $newUser = $this->user->find($userId);
        $temp = substr(Hash::make(rand(0,100)), 0, 10);
        $newPassword = Hash::make($temp);

        $newUser->password = $newPassword;
        $newUser->save();

        Mail::send('emails.passwordreset', ['newpassword' => $temp, 'username' => $username], function ($m) use ($email){
            $m->to($email)->subject('Password Reset');
        });
    }
}