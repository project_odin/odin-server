<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CompaniesModel;
use App\Models\PositionsModel;
use App\Models\LeaveInformationModel;
use App\Models\TaxExemptionModel;
use App\Models\DepartmentsModel;

class FilterController extends Controller
{

    function getFilters(Request $request)
    {   
        $filterMap = [
            'companies'   => new CompaniesModel(),
            'positions'   => new PositionsModel(),
            'leave_types' => new LeaveInformationModel(),
            'tax_exemption' => new TaxExemptionModel(),
            'departments' => new DepartmentsModel()
        ];

        $request = $request->all();
        $filters = $request['filters'];

        $filterResponse = [];

        foreach ($filters as $key => $value) {
            $model = $filterMap[$value];
            $filterResponse[$value] = $model->fetchAll();
        }

        return response()->json(['filters' => $filterResponse]);
    }
}
