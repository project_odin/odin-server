<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Hash;
use App\Models\Profile;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {}

    public function store(Request $request)
    {}

    public function show($id)
    {}

    public function update(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'first_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
            ]);

            $params = $request->all();
            $profile = Profile::find($id);
            $profile->first_name = $params['first_name'];
            $profile->middle_name = $params['middle_name'];
            $profile->last_name = $params['last_name'];
            $profile->save();

            return Response(array('status' => 'success', 'message' => 'Update profile success.'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {}
}
