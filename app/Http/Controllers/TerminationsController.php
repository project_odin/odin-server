<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ResignationsModel;
use App\Models\EmployeeModel;

class TerminationsController extends Controller
{

    public function index()
    {
        $resignations = ResignationsModel::all();
        return response()->json(['resignations' => $resignations]);
    }

    public function store(Request $request)
    {}

    public function show($id)
    {}

    public function update(Request $request, $id)
    {}

    public function destroy($id)
    {}
}
