<?php

namespace App\Http\Controllers\Applicant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ResumeModel;
use App\Models\ApplicantModel;

class ResumeController extends Controller
{

    protected $resume;
    protected $applicant;

    public function __construct(ResumeModel $resume, ApplicantModel $applicant)
    {
        $this->resume = $resume;
        $this->applicant = $applicant;
    }

    public function show($id)
    {
        $resume = $this->resume->fetch($id);
        return response()->json(['resumes' => $resume]);
    }

    public function destroy($id)
    {
        $this->resume->deleteResume($id);
        return response()->json(['success' => 'Resume deleted'], 200);
    }

    public function uploadResume(Request $request)
    {
        $params     = $request->all();
        $resume     = $request->file('file');

        if ($resume->getMimeType() === 'application/pdf') {

            $userId     = $params['user'];
            $title      = $params['title'];
            $isDefault  = $params['isDefault'];

            $applicant = $this->applicant->fetch($userId);


            $resumeData = array (
                'applicant_id'  => $userId,
                'title'         => $title,
                'is_default'    => $isDefault,
                'file'          => strtoupper($applicant->last_name) . '_' .  date('Ymdhis') . '.pdf',
            );

            $this->resume->fill($resumeData);
            $this->resume->save();

            $request->file('file')->move('public/candidates/resume', $resumeData['file']);

            $resumeId = $this->resume->id;

            if ($isDefault === 'y') {
                $this->resume->setDefault($userId, $resumeId);
            }

            return response()->json(['success' => 'Add resume success'], 200);

        } else {
            return response()->json(['error' => 'Invalid resume file type. PDF only.'], 412);
        }

    }

    public function setAsDefault($userId, $resumeId)
    {
         $this->resume->setDefault($userId, $resumeId);
    }
}
