<?php

namespace App\Http\Controllers\Applicant;

use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OpeningModel;
use App\Models\ResumeModel;
use App\Models\OpeningCandidatesModel;

class OpeningsController extends Controller
{

    protected $opening;
    protected $openingCandidates;
    protected $resume;

    public function __construct(
        OpeningModel $opening, 
        OpeningCandidatesModel $openingCandidates,
        ResumeModel $resume
    ) {
        $this->opening = $opening;
        $this->openingCandidates = $openingCandidates;
        $this->resume = $resume;
    }

    public function index()
    {
        $opening = $this->opening->fetchAll();

        return response()->json(['openings' => $opening]);
    }

    public function store(Request $request)
    {
        $applicant = $request->all();

        $resume = $this->resume->fetch($applicant['applicant']);

        $hasApplication = $this->openingCandidates->hasApplication($applicant['opening'], $applicant['applicant']);

        if ($hasApplication) {
            return response()->json(['code' => 'application_exists', 'message' => 'You have applied for this position.'], 412);
        }

        if (count($resume) > 0) {

            $params = [
                'opening_id' => $applicant['opening'],
                'candidate_id' => $applicant['applicant'],
                'status' => 's'
            ];

            $this->openingCandidates->fill($params);
            $this->openingCandidates->save();



            return response()->json(['code' => 'application_success'], 200);
            
        } else {
            return response()->json(['code' => 'no_resume_uploaded'], 412);
        }


    }

    public function show($id)
    {
        $opening = $this->opening->fetch($id);
        return response()->json(['opening' => $opening]);
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }

    public function fetch($type)
    {
        $opening = $this->opening->getOpenings($type);
        return response()->json(['openings' => $opening]);
    }
}
