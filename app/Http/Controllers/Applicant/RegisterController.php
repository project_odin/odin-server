<?php

namespace App\Http\Controllers\Applicant;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Applicant;
use App\Models\Profile;

class RegisterController extends Controller
{       

    protected $profile;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:applicants',
            'password' => 'required|min:8'
        ]);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

        $users      = new User();
        $applicants = new Applicant();

        $data = $request->all();

        $loginData = array (
            'username'  => $data['email'],
            'password'  => Hash::make($data['password']),
            'client_id' => 'applicant',
            'is_deleted' => 'n',
        );

        $users->fill($loginData);
        $users->save();

        $userId = $users->id;

        $applicantDetails = array (
            'id'         => $userId,
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'], 
            'email'      => $data['email'],
            'status'     => 'n'
        );

        $profileDetails = array(
            'user_id'   => $userId,
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'], 
        );

        $applicants->fill($applicantDetails);
        $applicants->save();

        $this->profile->fill($profileDetails);
        $this->profile->save();

        return Response(array('status' => 'success'), 200);
    }

    /**
     * Validates a username
     * 
     * @param  Strign $username 
     * @return Boolean           
     */
    public function validateUsername(Request $request)
    {
        $username = $request->input('username');

        $user = new User();
        
        $isExisting = $user->checkExistingUsername($username);

        if ($isExisting) {
            return Response(
                array(
                    'status' => 'existing'
                ),
                409
            );
        }
        return Response(array('status' => 'success'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
}
