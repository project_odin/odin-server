<?php

namespace App\Http\Controllers\Applicant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ApplicantModel;
use App\Models\Profile;

class ApplicantController extends Controller
{     

    protected $applicant; 
    protected $profile; 

    public function __construct(ApplicantModel $applicant, Profile $profile)
    {
        $this->applicant = $applicant;
        $this->profile = $profile;
    }

    public function show($id)
    {
        $applicant = $this->applicant->fetch($id);

        return response()->json(compact('applicant'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $profileData = array(
            'address'        => $data['address'],
            'birth_place'    => $data['birth_place'],
            'birthdate'      => $data['birthdate'],
            'cellphone'      => $data['cellphone'],
            'citizensip'     => $data['citizensip'],
            'email'          => $data['email'],
            'field'          => $data['field'],
            'first_name'     => $data['first_name'],
            'last_name'      => $data['last_name'],
            'middle_name'    => $data['middle_name'],
            'nick'           => $data['nick'],
            'religion'       => $data['religion'],
            'school'         => $data['school'],
            'telephone'      => $data['telephone'],
            'year_graduated' => $data['year_graduated']
        );

        $this->applicant->changeStatus($id, 'o');

        $this->profile->updateProfile($id, $profileData);

        return Response(array('status' => 'success'), 200);
        
    }
}
