<?php

namespace App\Http\Controllers\Applicant;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OpeningCandidatesModel;

class ApplicationController extends Controller
{
    protected $openingCandidates;

    public function __construct(OpeningCandidatesModel $openingCandidates)
    {
        $this->openingCandidates = $openingCandidates;
    }
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $applications = $this->openingCandidates->fetch($id);
        return response()->json(compact('applications'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
