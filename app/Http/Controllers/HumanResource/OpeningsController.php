<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OpeningModel;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\AuditTrailModel as Audit;

class OpeningsController extends Controller
{

    protected $opening;

    public function __construct(OpeningModel $opening)
    {
        $this->opening = $opening;
    }

    public function index()
    {
        $opening = $this->opening->getOpenings(null, true);

        return response()->json(['openings' => $opening]);
    }

    public function store(Request $request)
    {
        $opening = $request->all();
        
        $data = array(
            'title'             => $opening['title'], 
            'opening_type'      => $opening['type'], 
            'location'          => $opening['location'], 
            'job_description'   => $opening['description'], 
            'qualifications'    => $opening['qualifications'], 
            'vacancies'         => $opening['vacancies'], 
            'company_id'        => $opening['company_id'],
            'status'            => 'unpublished'
        );

        $this->opening->fill($data);
        $this->opening->save();

        $logger = JWTAuth::parseToken()->authenticate();
        Audit::saveAudit($logger['attributes']['id'], "Created an job opening (Opening ID : {$this->opening->id})");

        return Response(array('status' => 'success'), 200);

    }

    public function show($id)
    {
        $opening = $this->opening->with('companies')->findOrFail($id);
        return response()->json(['opening' => $opening]);
    }

    public function update(Request $request, $id)
    {

        try {

            $opening = $this->opening->findOrFail($id);

            $params = $request->all();

            $opening->title           = $params['title'];
            $opening->opening_type    = $params['opening_type'];
            $opening->location        = $params['location'];
            $opening->job_description = $params['job_description'];
            $opening->qualifications  = $params['qualifications'];
            $opening->vacancies       = $params['vacancies'];
            $opening->company_id      = $params['company_id'];

            $opening->save();

            return Response(array('status' => 'success', 'message' => 'Update job opening success.'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function destroy($id)
    {}

    public function publishJobPost($id)
    {
        $opening = $this->opening->find($id);
        $opening->status = 'published';
        $opening->save();
        return Response(array('message' => 'Job post publihed'), 200);
    }

    public function unpublishJobPost($id)
    {
        $opening = $this->opening->find($id);
        $opening->status = 'unpublished';
        $opening->save();
        return Response(array('message' => 'Job post unpublihed'), 200);
    }
}
