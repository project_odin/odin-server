<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Models\LeaveRequestsModel;
use App\Models\TimesheetModel;
use App\Models\OvertimeRequestsModel;

class TimesheetController extends Controller
{

    protected $leaveRequest;
    protected $timesheet;
    protected $overtimeRequest;

    public function __construct(
        LeaveRequestsModel $leaveRequest, 
        TimesheetModel $timesheet,
        OvertimeRequestsModel $overtimeRequest
    ) {
        $this->leaveRequest = $leaveRequest;
        $this->timesheet    = $timesheet;    
        $this->overtimeRequest    = $overtimeRequest;    
    }

    public function index()
    {
        $timesheets = $this->timesheet->fetchAll();
        return response()->json(['timesheets' => $timesheets]);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {

        $attendance = $request->file('file');

        $filename = date('Ymd') . '.xlsx';
        $request->file('file')->move('public/attendance/', $filename);

        // if ($attendance->getMimeType() === 'application/octet-stream' || 
            // $attendance->getMimeType() === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {


        Excel::selectSheets('Sheet1')->load('public/attendance/' . $filename, function($reader) {
            $results = $reader->get();

            $data = [];

            foreach ($results as $key => $value) {

                $timesheet = new TimesheetModel();

                $totalTime = 0.00;

                $isAbsent = filter_var($value->absent, FILTER_VALIDATE_BOOLEAN);

                if ($isAbsent) {
                    if ($this->leaveRequest->hasApprovedLeaveRequest($value->emp_no, $value->date)) {
                        $totalTime = 8.00;
                    }
                    $value->absent = 'y';
                } else {
                    $workTime = $this->timeToNumeric($value->work_time);

                    if ((double)$workTime > 8) {
                        $totalTime = 8.00;

                        if ($this->overtimeRequest->hasApprovedOvertimeRequest($value->emp_no, $value->date)) {
                            $value->ndays_ot = (double)$value->ndays_ot + ($workTime - 8);
                        } else {
                            $value->ndays_ot = 0.00;
                        }

                    } else {
                        $totalTime = $workTime;
                    }

                    $value->absent = 'n';
                }
                
                // Set net to totaltime
                $netTime = $totalTime;
                $total = 0;
                // If employee logs on time or late
                // Deduct the late from total time and early out
                if ((double)$this->timeToNumeric($value->clock_in) >= 8.00) {
                    $netTime = $totalTime - (((double)$this->timeToNumeric($value->late)) + ((double)$this->timeToNumeric($value->early)));
                } 

                $timesheet->employee_number = $value->emp_no;
                $timesheet->login_date      = date('Y-m-d', strtotime($value->date));
                $timesheet->log_in_time     = $value->clock_in;
                $timesheet->log_out_time    = $value->clock_out;
                $timesheet->hours_late_in   = $this->timeToNumeric($value->late);
                $timesheet->hours_early_out = $this->timeToNumeric($value->early);
                $timesheet->is_absent       = $value->absent;
                $timesheet->total_time      = (double)number_format($totalTime, 2);
                $timesheet->net_time        = (double)number_format($netTime, 2);
                $timesheet->over_time       = (double)number_format($value->ndays_ot, 2);

                $total += $timesheet->save();
            }
        });
        
        return response()->json(['success' => 'Timesheet upload success'], 200);
// 
        // } else {
            // return response()->json(['error' => 'Invalid file type. Excel file only.'], 412);
        // }

    }

    private function timeToNumeric($time) 
    {
        if ($time) {
            $arr = explode(':', $time);
            $hours   = (int)$arr[0];
            $minutes = (int)$arr[1] / 60;
            $total = $hours + $minutes;
            return number_format ($total, 2);
        }
        return number_format(0, 2);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
