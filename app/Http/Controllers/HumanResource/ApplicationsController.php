<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use App\Models\OpeningCandidatesModel;
use App\Models\EmployeeModel;
use App\Models\ApplicantModel;
use App\Models\OpeningModel;
use App\Models\UserModel;
use App\Models\Profile;
use Mail;
use JWTAuth;
use App\Models\AuditTrailModel as Audit;

class ApplicationsController extends Controller
{   
    protected $openingCandidates;
    protected $applicant;
    protected $employee;
    protected $opening;
    protected $user;
    protected $profile;

    public function __construct(
        OpeningCandidatesModel $openingCandidates, 
        ApplicantModel $applicant,
        EmployeeModel $employee,
        OpeningModel $opening,
        UserModel $user,
        Profile $profile
    ) {
        $this->openingCandidates = $openingCandidates;
        $this->applicant = $applicant;
        $this->employee = $employee;
        $this->opening = $opening;
        $this->user = $user;
        $this->profile = $profile;
    }

    public function index()
    {
        $candidates = $this->openingCandidates->fetchAll();
        return response()->json(['candidates' => $candidates]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $candidate = $this->openingCandidates->fetch($id);
        return response()->json(['candidate' => $candidate]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $params = $request->all();

        $applicantId = $id;
        $status      = $params['status'];
        $newStatus   = ''; 

        if ($status === 'j') {

            $applicant = $this->applicant->fetch($applicantId);

            $email = $applicant->email;

            $firstName = $applicant->first_name;
            $lastName = $applicant->last_name;

            $employeeId = date('y-md') . sprintf('%02d', (EmployeeModel::whereRaw('DATE_FORMAT(created_at, "%Y-%m-%d") = "'.date('Y-m-d').'"')->count() + 1));

            $user = [
                'username'   => $employeeId, 
                'password'   => Hash::make(strtoupper(str_replace(' ', '', $lastName))),
                'client_id'  => 'employee',
                'is_deleted' => 'n',
                'is_active'  => 'y'
            ];

            $this->user->fill($user);
            $this->user->save();
            $userId = $this->user->id;
            
            $profile = [
                'user_id'        => $userId,
                'first_name'     => isset($firstName) ? $firstName : null, 
                'middle_name'    => null, 
                'last_name'      => isset($lastName) ? $lastName : null,
                'profile_status' => 'n'
            ];

            $this->profile->fill($profile);
            $this->profile->save();

            $profileId = $this->profile->id;

            $employee = [
                'profile_id'         => $profileId,
                'user_id'            => $userId,
                'exemption_id'       => 0,
                'employee_number'    => $employeeId,
                'department_id'      => 0,
                'position_id'        => 0,
                'hire_date'          => date('Y-m-d'),
                'endo_date'          => date('Y-m-d', strtotime('+6 months'))
            ];
            
            $this->employee->fill($employee);
            $this->employee->save();

            $applicant->username = $employeeId; 

            Mail::send('email.hire', ['applicant' => $applicant], function ($m) use ($email){
                $m->to($email)->subject('You\'re Hired!');
            });

            $this->openingCandidates->moveStep($applicantId,  $params['opening_id'], 'h');

            return response()->json(['action' => 'move', 'data' => 'Applicant added to employee list.']);


        } else {
            
            $openingId   = $params['opening_id'];

            switch($status) {
                case 's' : $newStatus = 'p'; break;
                case 'p' : $newStatus = 'f'; break;
                case 'f' : $newStatus = 'j'; break;
            } 

            $this->openingCandidates->moveStep($applicantId, $openingId, $newStatus);

            $user = JWTAuth::parseToken()->authenticate();
            Audit::saveAudit($user['attributes']['id'], 'Applicant stage updated');

            return response()->json(['action' => 'move', 'data' => 'Applicant moved to next step.']);

        }
    }

    public function destroy($id)
    {
        //
    }
}
