<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\LeaveRequestsModel;
use JWTAuth;
use App\Models\AuditTrailModel as Audit;
class LeaveController extends Controller
{
    protected $leaveRequests;

    public function __construct(LeaveRequestsModel $leaveRequests)
    {
        $this->leaveRequests = $leaveRequests;
    }

    public function index()
    {
        $requests = $this->leaveRequests->getEmployeeLeaveRequests();
        return response()->json(['requests' => $requests]);
    }

    public function store(Request $request)
    {}

    public function show($id)
    {
        try {
            $leave = LeaveRequestsModel::findOrFail($id);
            $request = $this->leaveRequests->getLeave($id);
            return response()->json(['request' => $request]);

        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }
        
    }

    public function update(Request $request, $id)
    {
        try {
            $params = $request->all();
            $leave = LeaveRequestsModel::findOrFail($id);
            $leave->leave_status = $params['leave_status'];
            $leave->hr_remarks   = isset($params['remarks']) ? $params['remarks'] : null; 
            $leave->save();

            $user = JWTAuth::parseToken()->authenticate();
            Audit::saveAudit($user['attributes']['id'], 'Leave request updated.');
            
            return Response(array('status' => 'success'), 200);
        } catch(ModelNotFoundException $e) {
            return Response::make('Not Found', 404);
        }

    }
}
