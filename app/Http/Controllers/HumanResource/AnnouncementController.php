<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AnnouncementModel;   
use Illuminate\Database\Eloquent\ModelNotFoundException;
use JWTAuth;
use App\Models\AuditTrailModel as Audit;

class AnnouncementController extends Controller
{
    protected $announcement;

    public function __construct(AnnouncementModel $announcement)
    {
        $this->announcement = $announcement;
        // $this->middleware('jwt.auth');
    }

    public function index()
    {
        $announcements = $this->announcement->fetchAll();
        return response()->json(['announcements' => $announcements]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'   => 'required',
            'details' => 'required',
        ]);

        $params = $request->all();
        $user   = JWTAuth::parseToken()->authenticate();
        $userId = $user['attributes']['id'];

        $data = [
            'user_id' => $userId,
            'title'   => $params['title'],
            'details' => $params['details'],
            'status'  => 'u'
        ];

        $this->announcement->fill($data);
        $this->announcement->save();
    
        // Audit::saveAudit($user['attributes']['id'], 'Payroll created');

        // event(new \App\Events\CreateAnnnouncementEvent());
    }

    public function show($id)
    {
        try {
        
            $announcement = $this->announcement->findOrFail($id);
            return response()->json(['announcement' => $announcement]);
        
        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function update(Request $request, $id)
    {
        try {

            $this->validate($request, []);

            $params = $request->all();

            $params = $request->all();
            $announcement = $this->announcement->findOrFail($id);
            $announcement->title   = $params['title'];
            $announcement->details = $params['details'];
            $announcement->save();

            return Response(array('status' => 'success', 'message' => 'Update announcement success.'), 200);

        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);

        }
    }

    public function publishAnnouncement($id)
    {
        $announcement = $this->announcement->find($id);
        $announcement->status = 'p';
        $announcement->save();
        return Response(array('message' => 'Announcement publihed'), 200);
    }

    public function unpublishAnnouncement($id)
    {
        $announcement = $this->announcement->find($id);
        $announcement->status = 'u';
        $announcement->save();
        return Response(array('message' => 'Announcement unpublihed'), 200);
    }
}
