<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;
use Hash;
use Aloha\Twilio\Twilio;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\EmployeeModel;
use App\Models\Profile;
use App\Models\UserModel;
use App\Models\SalaryTableModel;
use JWTAuth;
use App\Models\AuditTrailModel as Audit;

class EmployeeController extends Controller
{
        protected $employee;
        protected $profile;
        protected $user;

    public function __construct(
        EmployeeModel $employee,
        Profile       $profile,
        UserModel     $user,
        SalaryTableModel $salary
    ) {
        $this->employee = $employee;
        $this->profile  = $profile;
        $this->user     = $user;
        $this->salary   = $salary;
    }

    public function index()
    {
        $employees = $this->employee->fetchAll();
        return response()->json(['employees' => $employees]);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'department_id'     => 'required',
                'position_id'       => 'required',
                'hire_date'         => 'required',
                'endo_date'         => 'required',
                'first_name'        => 'required',
                'last_name'         => 'required'
            ]);

            $params = $request->all();

            $employeeId = date('y-md') . sprintf('%02d', (EmployeeModel::whereRaw('DATE_FORMAT(created_at, "%Y-%m-%d") = "'.date('Y-m-d').'"')->count() + 1));

            $user = [
                'username'   => $employeeId, 
                'password'   => Hash::make(strtoupper(str_replace(' ', '', $params['last_name']))),
                'client_id'  => 'employee',
                'is_deleted' => 'n',
                'is_active'  => 'y'
            ];

            $this->user->fill($user);
            $this->user->save();
            $userId = $this->user->id;
            
            $profile = [
                'user_id'        => $userId,
                'first_name'     => isset($params['first_name']) ? $params['first_name'] : null, 
                'middle_name'    => isset($params['middle_name']) ? $params['middle_name'] : null, 
                'last_name'      => isset($params['last_name']) ? $params['last_name'] : null,
                'profile_status' => 'n'
            ];

            $this->profile->fill($profile);
            $this->profile->save();

            $profileId = $this->profile->id;

            $employee = [
                'profile_id'         => $profileId,
                'user_id'            => $userId,
                'exemption_id'       => $params['exemption_id'],
                'employee_number'    => $employeeId,
                'department_id'      => $params['department_id'],
                'position_id'        => $params['position_id'],
                'hire_date'          => $params['hire_date'],
                'endo_date'          => $params['endo_date']
            ];

            $this->employee->fill($employee);
            $this->employee->save();

            $salaryData = [
                'employee_id' => $employeeId, 
                'amount'      => 466.00,  
                'ecola'       => 15
            ]; 

            $this->salary->fill($salaryData);
            $this->salary->save();

            $user = JWTAuth::parseToken()->authenticate();
            Audit::saveAudit($user['attributes']['id'], 'Employee added.');

            return Response(array('status' => $userId), 200);   
        } catch (Exception $e) {
            return Response::make('Something went wrong', 404);                                    
        }
    }

    public function show($id)
    {
        $employee = $this->employee->getEmployee($id);
        $user     = $this->user->searchUser($id);

        if ($employee) {
            return response()->json(['employee' => $employee, 'user' => $user]);
        }
        
        return Response::make('Employee Not Found', 404);
    }

    public function update(Request $request, $id)
    {
            $params                       = $request->all();
            $employee                     = $this->employee->find($id);
            $employee->account_number     = $params['account_number'];
            $employee->phil_health_number = $params['phil_health_number'];
            $employee->sss_number         = $params['sss_number'];
            $employee->tin_number         = $params['tin_number'];
            $employee->pagibig_number     = $params['pagibig_number'];
            $employee->position_id        = $params['position_id'];
            $employee->department_id      = $params['department_id'];
            $employee->exemption_id       = $params['exemption_id'];
            $employee->hire_date          = $params['hire_date'];
            $employee->endo_date          = $params['endo_date'];
            $employee->save();

            return Response(array('message' => 'Update employee Success'), 200); 
    }

    public function destroy($id)
    {        
        try {
            $user = UserModel::findOrFail($id);
            $user->is_deleted = UserModel::DELETED;
            $user->save();
            return Response(array('status' => 'success', 'message' => 'User successfully deleted.'), 200);
        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);
        }
    }

    public function deactivate($id)
    {
        try {
            $user = UserModel::findOrFail($id);
            $user->is_active = UserModel::STATUS_INACTIVE;
            $user->save();
            return Response(array('status' => 'success', 'message' => 'User successfully deactivated.'), 200);
        } catch(ModelNotFoundException $e) {

            return Response::make('Not Found', 404);
        }
    }
}
