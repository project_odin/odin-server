<?php

namespace App\Http\Controllers\HumanResource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PayrollModel;
use App\Models\SalaryTableModel;

use JWTAuth;
use App\Models\AuditTrailModel as Audit;

class PayrollController extends Controller
{
    protected $payroll;
    protected $salaryTable;

    public function __construct(PayrollModel $payroll, SalaryTableModel $salaryTable)
    {
        $this->payroll = $payroll;
        $this->salaryTable = $salaryTable;
    }

    public function index()
    {
        $summary = $this->payroll->getPayrollSummary();

        return response()->json(['summary' => $summary]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $data = [
            'payroll_from'        => date('Y-m-d', strtotime($params['from'])),
            'payroll_to'          => date('Y-m-d',strtotime($params['to'])),
            'employee_number'     => $params['employee_number'],
            'basic_pay'           => preg_replace('/[ ,]+/', '', trim($params['basic_pay'])),
            'rate_per_hour'       => preg_replace('/[ ,]+/', '', trim($params['rate_per_hour'])),
            'hours'               => preg_replace('/[ ,]+/', '', trim($params['hours'])),
            'total_basic'         => preg_replace('/[ ,]+/', '', trim($params['total_basic'])),
            'overtime'            => preg_replace('/[ ,]+/', '', trim($params['overtime'])),
            'overtime_pay'        => preg_replace('/[ ,]+/', '', trim($params['overtime_pay'])),
            'night_diff'          => 0,
            // 'night_diff'          => preg_replace('/[ ,]+/', '', trim($params['night_diff'])),
            // 'night_diff_pay'      => preg_replace('/[ ,]+/', '', trim($params['night_diff_pay'])),
            'night_diff_pay'      => 0,
            'special_holiday_pay' => preg_replace('/[ ,]+/', '', trim($params['special_holiday_pay'])),
            'legal_holiday_pay'   => preg_replace('/[ ,]+/', '', trim($params['legal_holiday_pay'])),
            'ecola'               => preg_replace('/[ ,]+/', '', trim($params['ecola'])),
            'hdmf'                => preg_replace('/[ ,]+/', '', trim($params['hdmf'])),
            'philhealth'          => preg_replace('/[ ,]+/', '', trim($params['philhealth'])),
            'sss'                 => preg_replace('/[ ,]+/', '', trim($params['sss'])),
            'tax'                 => preg_replace('/[ ,]+/', '', trim($params['tax']))
        ];

        $this->payroll->fill($data);
        $this->payroll->save();

        $user = JWTAuth::parseToken()->authenticate();
        Audit::saveAudit($user['attributes']['id'], 'Payroll created');
        return response()->json(['success' => 'Payroll Saved'], 200);

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getContribution($baseSalary, $type)
    {       
        $contribution = [];

        switch($type) {
            case 'sss' : $contribution =  $this->payroll->getSSSContribution($baseSalary); break;
            case 'ph'  : $contribution =  $this->payroll->getPhilHealthContribution($baseSalary); break;
        }

        return response()->json(['contribution' => $contribution]);
    }

    public function computeWithholdingTax(Request $request)
    {
        $params = $request->all();
        $taxable = $params['amount'];
        $employee = $params['employee'];
        $taxExcemption =  $this->payroll->getException($employee, $taxable);
        $witholding = (($taxable - $taxExcemption[0]->salary_limit) * $taxExcemption[0]->percent_status) + $taxExcemption[0]->exemption;
        return response()->json(['tax' => $witholding]);
    }


    public function getEmployeeDetails(Request $request)
    {
        $params = $request->all();

        $employee = $params['employee'];
        $from     = $params['date_from'];
        $to       = $params['date_to'];
            
        $regularPay = $this->payroll->getHolidayPay($employee, $from, $to, 'special');
        $specialPay = $this->payroll->getHolidayPay($employee, $from, $to, 'regular');

        $regularHolidayPay = !empty($regularPay) ? $regularPay[0]->holiday_pay : 0;
        $specialHolidayPay = !empty($specialPay) ? $specialPay[0]->holiday_pay : 0;

        $result = $this->payroll->getEmployeeData($employee, $from, $to);

        

        $obj = new \StdClass();
        $obj->special_holiday_pay = $specialHolidayPay;
        $obj->regular_holiday_pay = $regularHolidayPay;

        $result = (object) array_merge((array) $result[0], (array) $obj);

        return response()->json(['employee' => $result]);

    }
}
