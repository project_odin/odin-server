<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Profile;

class AccountController extends Controller
{

    protected $user;
    protected $profile;

    public function show($id)
    {
        $profile = User::with('profiles')->find($id);
        return response()->json(['user' => $profile]);     
    }

    public function update(Request $request, $id)
    {
        //
    }
}
