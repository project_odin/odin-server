<?php


Route::get('/', function () {
    return bcrypt('password');;
});

/**
 * Authentication Routes
 */
Route::group(['prefix' => 'api'], function()
{
    Route::resource('user', 'UserController');
    Route::resource('account', 'AccountController');
    Route::resource('profile', 'ProfileController');
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

    /**
     * ---------------------------------------------------------------------
     * ADMIN RESOURCE ROUTES 
     * ---------------------------------------------------------------------
    */
    Route::group(['prefix' => 'admin'], function(){
        Route::resource('users', 'Admin\UserController');
        Route::resource('company', 'Admin\CompaniesController');
        Route::resource('position', 'Admin\PositionsController');
    });

    /**
     * ---------------------------------------------------------------------
     * HR ADMIN RESOURCE ROUTES 
     * ---------------------------------------------------------------------
     */          
    Route::group(['prefix' => 'hr'], function(){
        Route::resource('employee', 'HumanResource\EmployeeController');
        Route::resource('opening', 'HumanResource\OpeningsController');
        Route::resource('announcements', 'HumanResource\AnnouncementController');
        Route::get('publish/opening/{id}', 'HumanResource\OpeningsController@publishJobPost');
        Route::get('unpublish/opening/{id}', 'HumanResource\OpeningsController@unpublishJobPost');
        Route::get('publish/announcement/{id}', 'HumanResource\AnnouncementController@publishAnnouncement');
        Route::get('unpublish/announcement/{id}', 'HumanResource\AnnouncementController@unpublishAnnouncement');
        Route::resource('candidates', 'HumanResource\ApplicationsController');
        Route::resource('leave', 'HumanResource\LeaveController');
        Route::resource('announcement', 'HumanResource\AnnouncementController');
        Route::resource('payroll', 'HumanResource\PayrollController');
        Route::resource('resignation', 'ResignationsController');
        Route::resource('termination', 'TerminationsController');
        Route::resource('timesheet', 'HumanResource\TimesheetController');
        Route::get('payroll/contribution/{salary}/{type}', 'HumanResource\PayrollController@getContribution');
        
        Route::group(['prefix' => 'payroll'], function(){
            Route::post('employee', 'HumanResource\PayrollController@getEmployeeDetails');
            Route::post('tax', 'HumanResource\PayrollController@computeWithholdingTax');
        });

    });

    /**
     * ---------------------------------------------------------------------
     * CANDIDATE ROUTES
     * ---------------------------------------------------------------------
     */
    Route::group(['prefix' => 'candidate'], function(){
        Route::resource('applicant', 'Applicant\ApplicantController');
        Route::resource('application', 'Applicant\OpeningsController');
        Route::resource('apply', 'Applicant\ApplicationController');
        Route::resource('resume', 'Applicant\ResumeController');
        Route::resource('register', 'Applicant\RegisterController');
        Route::post('resume/upload', 'Applicant\ResumeController@uploadResume');
        Route::post('register/validate', 'Applicant\RegisterController@validateUsername');
        Route::get('openings/{type}', 'Applicant\OpeningsController@fetch');
        Route::put('resume/default/{user}/{id}', 'Applicant\ResumeController@setAsDefault');
    });

    /**
     * ---------------------------------------------------------------------
     * DATABASE BACKUP ROUTE
     * ---------------------------------------------------------------------
     */
    Route::get('backup/{id}/{type}', 'Admin\AdminController@backUpDatabase');
    Route::get('backup/', 'Admin\AdminController@getDbLogs');
    Route::get('audit', 'Admin\AdminController@getAuditTrail');


    Route::get('filters', 'FilterController@getFilters');


    /**
     * ---------------------------------------------------------------------
     * EMPLOYEE ROUTES
     * ---------------------------------------------------------------------
     */
    Route::group(['prefix' => 'employee'], function(){
        Route::resource('employee', 'Employee\EmployeeController');
        Route::resource('leave', 'Employee\LeaveController');
        Route::resource('ledger', 'Employee\PayrollController');
        Route::get('leave/list/{user}', 'Employee\LeaveController@getleaveRequests');
    });
    /**
     * ---------------------------------------------------------------------
     * DATABASE RESTORE ROUTE
     * ---------------------------------------------------------------------
     */

    Route::group(['prefix' => 'db'], function(){
        Route::post('backup', function() {
            $type   = Request::input('type');
            $userId = Request::input('user');

            $app = app();
            $controller = $app->make('App\Http\Controllers\Admin\AdminController');
            $controller->callAction('backUpDatabase', $parameters = array('id' => $userId, 'type' => $type));
            Artisan::call('db:backup');
        });

        Route::post('restore', function()
        {           
            return Artisan::call('db:restore');
        });
    });

    Route::get('user/reset-password/{username}', 'UserController@resetPassword');

    /**
     * ---------------------------------------------------------------------
     * REPORT ROUTES
     * ---------------------------------------------------------------------
     */
    Route::get('report', 'ReportController@generateReport');
});
