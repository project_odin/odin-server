<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use DB;
use File;
class DatabaseRestore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Database Backup Started');

        $server_name   = Config::get('database.connections.mysql.host');
        $username      = Config::get('database.connections.mysql.username');
        $password      = Config::get('database.connections.mysql.password');
        $database_name = Config::get('database.connections.mysql.database');
        $date_string   = date("Ymd");

        $backupPath     = 'public/public/';
        $backupFileName = $database_name . "-" . date("Y-m-d-H-i-s") . '.sql';

        // $cmd = "mysql -u root -p " . $database_name . " < " . $backupPath . '/' . $database_name . '.sql';
        // $cmd = "mysql --login-path=mysql.cnf  -e db_odin < " . $backupPath . '/' . $database_name . '.sql';
        $cmd = "mysql --defaults-extra-file=mysql.cnf db_odin < public/public/db_odin.sql";
        $arr_out = array();
        unset($return);

        system($cmd);
    }
}
