<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;

class DatabaseBackup extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup mySQL database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Database Backup Started');

        $server_name   = Config::get('database.connections.mysql.host');
        $username      = Config::get('database.connections.mysql.username');
        $password      = Config::get('database.connections.mysql.password');
        $database_name = Config::get('database.connections.mysql.database');
        $date_string   = date("Ymd");

        $backupPath     = 'public/';
        $backupFileName = $database_name . '.sql';

        $cmd = "mysqldump --hex-blob --routines --skip-lock-tables --log-error=mysqldump_error.log -h {$server_name} -u {$username} -p{$password} {$database_name} > " . $backupPath . $backupFileName;

        $arr_out = array();
        unset($return);

        exec($cmd, $arr_out, $return);

        if($return !== 0) {
            echo "mysqldump for {$server_name} : {$database_name} failed with a return code of {$return}\n\n";
            echo "Error message was:\n";
            $file = escapeshellarg("mysqldump_error.log");
            $message = `tail -n 1 $file`;
            echo "- $message\n\n";
        }

    }
}
