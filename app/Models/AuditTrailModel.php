<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AuditTrailModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'audit_trail';

    protected $fillable = ['user_id', 'action_taken'];

    public static function saveAudit($userId, $action)
    {
        return DB::table('audit_trail')
            ->insert(
                ['user_id' => $userId, 'action_taken' => $action, 'created_at' => date('Y-m-d h:i:s')]
            );
    }

    public function fetchAll()
    {
        $logs = DB::table($this->table)
            ->join('profiles', 'profiles.user_id', '=', 'audit_trail.user_id')
            ->select(
                'audit_trail.*',
                'profiles.first_name',
                'profiles.middle_name', 
                'profiles.last_name')
            ->orderBy('audit_trail.id', 'desc')
            ->get();

        return $logs;
    }
}