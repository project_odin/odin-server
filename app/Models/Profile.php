<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

class Profile extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'first_name', 
        'middle_name', 
        'last_name',
        'address',
        'birth_place',
        'birthdate',
        'cellphone',
        'citizensip',
        'email',
        'field',
        'nick',
        'religion',
        'school',
        'telephone',
        'year_graduated',
        'profile_status'
    ];

    /**
     * Check if there are any existing username in users table
     * 
     * @param  String
     * @return array
     */
    public function checkExistingUsername($username)
    {
        $users = DB::table('users')
            ->where('username', $username)
            ->count();

        return $users;
    }

    public function updateProfile($userId, $data)
    {
        DB::table($this->table)
            ->where('user_id', $userId)
            ->update($data);
    }
}
