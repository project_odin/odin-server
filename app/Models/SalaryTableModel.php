<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryTableModel extends Model
{
    protected $table = 'salary_table';
    protected $fillable = ['employee_id', 'amount', 'ecola'];
}
