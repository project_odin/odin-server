<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class EmployeeModel extends Model
{
    protected $table = 'employees';
    protected $alias = ' AS e';

    protected $fillable = [
		'profile_id',
		'user_id',
		'exemption_id',
		'employee_type_id',
		'employee_number',
		'contact_number',
		'civil_status',
		'gender',
		'address',
		'birth_date',
		'department_id',
		'position_id',
		'hire_date',
		'endo_date',
		'account_number',
		'phil_health_number',
		'sss_number',
		'tin_number',
		'pagibig_number'
	];

	public function fetchAll()
	{
		return DB::table($this->table . $this->alias)
			->select(
				'e.employee_number',
				'p.first_name',
				'p.last_name',
				'pos.position',
				'dept.department',
				'e.hire_date'
			)
			->leftJoin('users as u', 'u.id', '=', 'e.user_id')
			->leftJoin('profiles as p', 'p.id', '=', 'e.profile_id')
			->leftJoin('positions as pos', 'pos.id', '=', 'e.position_id')
			->leftJoin('departments as dept', 'dept.id', '=', 'e.department_id')
			->where('u.is_deleted', '=', 'n')
			->get();
	}

	public function getEmployee($id)
	{
		return DB::table($this->table . $this->alias)
			->select(
				'e.*',
				'e.id AS employee_id',
				'p.*',
				'pos.*',
				'dept.*',
				'te.*',
				'e.hire_date',
				'st.amount'
			)
			->join('users as u', 'u.id', '=', 'e.user_id')
			->leftJoin('profiles as p', 'p.id', '=', 'e.profile_id')
			->leftJoin('positions as pos', 'pos.id', '=', 'e.position_id')
			->leftJoin('departments as dept', 'dept.id', '=', 'e.department_id')
			->leftJoin('salary_table as st', 'st.employee_id', '=', 'e.employee_number')
			->leftJoin('tax_excemptions as te', 'te.id', '=', 'e.exemption_id')
			->where('employee_number', '=', $id)
			->get();
	}

	public function getEmployees($params)
	{
		$query = DB::table($this->table . $this->alias)->select();

		if (!empty($params['department'])) {
			$query->where('department_id', '=', $params['department']);			
		}

		if (!empty($params['employee_type'])) {
			$query->where('employee_type_id', '=', $params['employee_type']);			
		}

		if (!empty($params['position'])) {
			$query->where('position_id', '=', $params['position']);			
		}

		if (!empty($params['date_hired'])) {
			$query->where('hire_date', '=', $params['date_hired']);
		}

		$query->join('users as u', 'u.id', '=', 'e.user_id');
		$query->join('profiles as p', 'p.id', '=', 'e.profile_id');
		$query->join('positions as pos', 'pos.id', '=', 'e.position_id');
		$query->join('departments as dept', 'dept.id', '=', 'e.department_id');
		$query->join('salary_table as st', 'st.employee_id', '=', 'e.employee_number');
		$query->where('u.is_deleted', '=', 'n');
		
		return $query->get();

	}

	public function profiles()
	{
		return $this->hasOne('App\Models\Profile', 'id', 'profile_id');
	}

	public function positions()
	{
		return $this->hasOne('App\Models\PositionsModel', 'id', 'position_id');
	}

	public function departments()
	{
		return $this->hasOne('App\Models\DepartmentsModel', 'id', 'department_id');
	}

	public function exemptions()
	{
		return $this->hasOne('App\Models\TaxExemptionModel', 'id', 'exemption_id');
	}
}