<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimesheetModel extends Model
{
    protected $table = 'employee_attendance';
    protected $fillable = [
        'employee_number', 
        'login_date', 
        'log_in_time', 
        'log_out_time', 
        'hours_late_in', 
        'hours_early_out', 
        'is_absent', 
        'total_time', 
        'net_time', 
        'over_time'
    ];

    public function employees()
    {
        return $this->belongsTo('App\Models\EmployeeModel', 'employee_number', 'employee_number');
    }
    
    public function fetchAll()
    {
        return $this->with('employees.profiles')->get();
    }
}
