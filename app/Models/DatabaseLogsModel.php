<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DatabaseLogsModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'db_logs';

    protected $fillable = ['user_id', 'log_type'];

    public function saveDbLog($userId, $type)
    {
    	$date = date('Y-m-d h:i:s');

        try {
            DB::table($this->table)
                ->where('user_id', $userId)
                ->where('log_type', $type)
                ->update(array('updated_at' => $date));
        } catch(Exception $e) {
            Db::table($this->table)
                ->insert(
                    ['user_id' => $userId, 'log_type' => $type, 'updated_at' => $date]
                );
        }
    }

    public function getLogs()
    {
        return DB::table($this->table)->select()->get();
    }
}