<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PayrollModel extends Model
{
    protected $sssLookup = 'sss_contribution_lookup'; 
    protected $phLookup  = 'philhealth_contribution_lookup'; 
    protected $taxtable  = 'tax_table';

    protected $table = 'payroll';
    protected $fillable = [
        'payroll_from',
        'payroll_to',
        'employee_number',
        'basic_pay',
        'rate_per_hour',
        'hours',
        'overtime',
        'total_basic',
        'overtime_pay',
        'night_diff',
        'night_diff_pay',
        'special_holiday_pay',
        'legal_holiday_pay',
        'ecola',
        'hdmf',
        'philhealth',
        'sss',
        'tax'];


    public function getSSSContribution($baseSalary)
    {       
        $baseSalary = preg_replace('/[ ,]+/', '', trim($baseSalary));

        return DB::table($this->sssLookup)
            ->select('employee_contribution AS contribution')
            ->whereRaw("'$baseSalary' BETWEEN sss_contribution_lookup.range_from AND sss_contribution_lookup.range_to")
            ->get();
    }

    public function getPhilHealthContribution($baseSalary)
    {       
        $baseSalary = preg_replace('/[ ,]+/', '', trim($baseSalary));

        return DB::table($this->phLookup)
            ->select('employee_share AS contribution')
            ->whereRaw("'$baseSalary' BETWEEN philhealth_contribution_lookup.range_from AND philhealth_contribution_lookup.range_to")
            ->get();   
    }

    public function getEmployeeData($employee, $from, $to)
    {
        $rawQuery = "SELECT 
                employee_number,
                SUM(total_time) AS hours_rendered, 
                SUM(over_time) AS total_overtime, 
                SUM(hours_late_in) AS total_late
            FROM employee_attendance WHERE employee_number = '$employee' 
            AND DATE_FORMAT(login_date, '%Y-%m-%d') BETWEEN '$from' AND '$to'
            GROUP BY employee_number";

        return DB::table('employees as e')
            ->select(
                'e.employee_number',
                'p.first_name',
                'p.last_name',
                'st.amount',
                'hours.hours_rendered',
                'hours.total_overtime',
                'hours.total_late'
            )
            ->join('users as u', 'u.id', '=', 'e.user_id')
            ->join('profiles as p', 'p.id', '=', 'e.profile_id')
            ->join('salary_table as st', 'st.employee_id', '=', 'e.employee_number')
            ->join(DB::raw("($rawQuery) as hours"), function($join)
            {
                $join->on('e.employee_number', '=', 'hours.employee_number');
            })
            ->where('e.employee_number', '=', $employee)
            ->get();
    }

    public function getHolidayPay($employee, $from, $to, $type)
    {
            
        if ($type === 'special') {
            $payQuery     = "SELECT ((amount * 1.3) + 15) / 8 FROM salary_table WHERE employee_id = '$employee'";
        } else {
            $payQuery     = "SELECT ((amount  +  15 ) * 2) / 8 FROM salary_table WHERE employee_id = '$employee'";
        }


        $holidayQuery = "SELECT holiday_date FROM holidays WHERE holiday_type = '$type'";
        
        return DB::table('employee_attendance as ea')
            ->select(DB::raw("total_time * ($payQuery) as holiday_pay"))
            ->whereIn(DB::raw("DATE_FORMAT(login_date, '%Y-%m-%d')"), function($query) use ($type)
            {
                $query->select(DB::raw('holiday_date'))
                    ->from('holidays')
                    ->where('holiday_type', '=', $type);
            })
            ->get();
    }

    public function getException($employee, $salary)
    {
        return DB::table('tax_table')
            ->where('exemption_id', '=', DB::raw("(SELECT exemption_id FROM employees WHERE employee_number =  '$employee')"))
            ->where('salary_limit', '<', $salary)
            ->orderBy('salary_limit', 'DESC')
            ->take(1)
            ->get();
    }

    public function getPayrollDetails($id)
    {
        return DB::table($this->table)
            ->join('employees as e', 'e.employee_number', '=', 'payroll.employee_number')
            ->join('profiles as p', 'p.id', '=', 'e.profile_id')
            ->select('e.*', 'p.*', 'payroll.*', 
                DB::raw('(payroll.total_basic 
                    + payroll.overtime_pay 
                    + payroll.special_holiday_pay 
                    + payroll.legal_holiday_pay
                    + payroll.ecola
                    + payroll.night_diff_pay) as total_earning,

                    (payroll.sss 
                    + payroll.philhealth
                    + payroll.hdmf
                    + payroll.tax) as total_deduction'))
            ->where('payroll.employee_number', '=', $id)->get();
    }

    public function getPayrollSummary($params = null)
    {
        $query  = DB::table($this->table)
            ->join('employees as e', 'e.employee_number', '=', 'payroll.employee_number')
            ->join('profiles as p', 'p.id', '=', 'e.profile_id')
            ->select('e.*', 'p.*', 'payroll.*', 
                DB::raw('(payroll.total_basic 
                    + payroll.overtime_pay 
                    + payroll.special_holiday_pay 
                    + payroll.legal_holiday_pay
                    + payroll.ecola
                    + payroll.night_diff_pay) as total_earning,

                    (payroll.sss 
                    + payroll.philhealth
                    + payroll.hdmf
                    + payroll.tax) as total_deduction'));

        if (isset($params)) {
            $query->where('payroll_from', '=', $params['date_from']);
            $query->where('payroll_to', '=', $params['date_to']);
        }

        return $query->get();
    }

}