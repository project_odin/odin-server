<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TerminationsModel extends Model
{
    protected $table = "terminations";

    protected $fillable = ["employee_number", "termination_date", "description", "note", "status", "delete_flag"];

    protected $alias = "as t";
}
