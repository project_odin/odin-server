<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TaxExemptionModel extends Model
{
    protected $table = 'tax_excemptions';

    protected $fillable = ['excemption', 'description'];

    public function fetchAll()
    {
        return DB::table($this->table)->select()->get();
    }
}
