<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OpeningModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'openings';

    protected $alias = ' AS o ';

    protected $fillable = [
        'title', 
        'opening_type', 
        'location', 
        'job_description', 
        'qualifications', 
        'company_id', 
        'status'
    ];

    public function getOpenings($type = null, $isAdmin = false)
    {

        $openings = DB::table($this->table . $this->alias)
            ->select(DB::raw('o.*, COUNT(oc.opening_id) AS candidates, (SELECT count(*) FROM opening_candidates WHERE opening_id = o.id) as applicatons'))
            ->leftJoin('opening_candidates AS oc', 'oc.opening_id', '=', 'o.id')
            ->groupBy('o.id');

        if (isset($type)) {
            if ($type == 'latest') {
                $openings->orderBy('o.created_at', 'desc');
            } else if ($type == 'top') {
                $openings->orderBy('candidates', 'asc');
                $openings->take(5);
            }
        }

        if (!$isAdmin) {
            $openings->whereRaw("o.status = 'published'");
        }
        
        return DB::select(DB::raw($openings->toSql()));

    }
    public function fetchAll()
    {
        $openings = DB::table($this->table)->where('status', '=', 'published')->toSql();
        return $openings;
    }

    public function fetch($openingId)
    {
        return DB::table($this->table)
            ->where('id', '=', $openingId)
            ->get();
    }

    public function companies()
    {
        return $this->hasOne('App\Models\CompaniesModel', 'id', 'company_id');
    }
}