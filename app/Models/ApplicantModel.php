<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApplicantModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applicants';

    protected $fillable = ['username', 'password', 'client_id'];

    public function fetch($userId)
    {
        $users = DB::table($this->table)
            ->select(
                'first_name',
                'last_name', 
                'email', 
                'status'
            ) 
            ->where('id', '=', $userId)
            ->first();

        return $users;
    }

    public function changeStatus($id, $status)
    {
        DB::table($this->table)
            ->where('id', $id)
            ->update(array('status' => $status));
    }
}