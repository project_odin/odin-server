<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{   
    const STATUS_INACTIVE = 'n';
    const STATUS_ACTIVE   = 'y';
    const DELETED         = 'y';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $fillable = ['username', 'password', 'client_id', 'is_active', 'is_deleted'];

    /**
     * Check if there are any existing username in users table
     * 
     * @param  String
     * @return array
     */
    
    public function checkExistingUsername($username)
    {
        $users = DB::table('users')
            ->where('username', $username)
            ->count();

        return $users;
    }

    public function fetchAll()
    {
        $users = DB::table($this->table)
            ->join('profiles', 'profiles.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.username',
                'users.client_id',
                'profiles.first_name',
                'profiles.middle_name', 
                'profiles.last_name',
                'profiles.created_at')
            ->where('is_deleted', '=', 'n')
            ->get();

        return $users;
    }

    public function fetch($userId)
    {
        $users = DB::table($this->table)
            ->join('profiles', 'profiles.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.username',
                'users.client_id',
                'profiles.first_name',
                'profiles.middle_name', 
                'profiles.last_name',
                'profiles.created_at')
            ->where('users.id', '=', $userId)
            ->where('is_deleted', '=', 'n')
            ->first();

        return $users;
    }

    public function getUserDetails($user)
    {
        $userType = $user['client_id'];

        $select =  DB::table($this->table)
            ->join('profiles', 'profiles.user_id', '=', 'users.id')
            ->where('users.id', '=', $user['id']);

        if ($userType === 'employee') {
            $select->join('employees as e', 'e.user_id', '=', 'users.id');
            $select->join('departments as d', 'd.id', '=', 'e.department_id');
            $select->join('positions as p', 'p.id', '=', 'e.position_id');
            $select->join('tax_excemptions as t', 't.id', '=', 'e.exemption_id');
        }

        return $select->get();
    }

    public function searchUser($username)
    {
        return DB::table($this->table)->where('username', '=', $username)->get();
    }
}