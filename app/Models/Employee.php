<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $fillable = [
        'profile_id', 
        'user_id', 
        'contact_nuber', 
        'civil_status', 
        'gender', 
        'address', 
        'birthdate', 
        'depertment_id',
        'position_id',
        'hire_date',
        'endo_date',
        'account_number',
        'phil_health_number',
        'sss_number',
        'tin_number',
        'pagibig_number'
    ];
}
