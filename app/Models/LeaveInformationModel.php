<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LeaveInformationModel extends Model
{
    protected $table = 'leave_information';
    
    protected $fillable = ['leave_type'];

    public function fetchAll()
    {
        return DB::table($this->table)->select()->get();
    }
}
