<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AnnouncementModel extends Model
{
    protected $table = "announcements";
    protected $alias =  " as a";

    protected $fillable = ['user_id', 'title', 'details', 'status'];

    public function fetchAll()
    {
        return DB::table($this->table . $this->alias)
            ->join('profiles as p', 'p.user_id', '=', 'a.user_id')
            ->select(
                'a.*',
                'p.first_name',
                'p.middle_name', 
                'p.last_name')
            ->orderBy('a.id', 'desc')
            ->get();
    }

}
