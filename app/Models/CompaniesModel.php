<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class CompaniesModel extends Model
{
    protected $table = 'companies';

    protected $fillable = ['id', 'company_code', 'name', 'location', 'contact_person'];

    public function fetchAll()
    {
        return DB::table($this->table)->select()->get();
    }
}
