<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResignationsModel extends Model
{
    protected $table = "resignations";

    protected $fillable = ["employee_number", "date_notice", "resignation_date", "reason", "note", "status", "delete_flag"];

    protected $alias = "as r";

    public function employees()
    {
        return $this->belongsTo('App\Models\EmployeeModel', 'employee_number', 'employee_number');
    }

    public function fetchAll()
    {
        return $this->with('employees.profiles')->where('delete_flag', 'n')->get();
    }

    public function fetch($id)
    {
        return $this->with("employees.profiles")->findOrFail($id);
    }

    public function saveResignation($data)
    {
        $this->employee_number  = $data['employee_number']; 
        $this->date_notice      = $data['date_notice']; 
        $this->resignation_date = $data['resignation_date']; 
        $this->reason           = $data['reason']; 
        $this->note             = $data['note']; 
        $this->status           = 'p';        
        $this->delete_flag      = 'n';  
        $this->save();      
    }

    public function updateResignation($data, $id)
    {
        $resignation                   = $this->findOrFail($id);
        $resignation->date_notice      = $data['date_notice']; 
        $resignation->resignation_date = $data['resignation_date']; 
        $resignation->reason           = $data['reason']; 
        $resignation->note             = $data['note']; 
        $resignation->save();
    }

    public function deleteResignation($id)
    {
        $resignation = $this->findOrFail($id);      
        $resignation->delete_flag = 'y';
        $resignation->save();
    }
}
