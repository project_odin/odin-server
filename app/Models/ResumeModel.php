<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;

class ResumeModel extends Model
{

    protected $table = 'resume';

    protected $fillable = ['applicant_id', 'title', 'file', 'is_default'];

    public function setDefault($applicantId, $resumeId)
    {
        DB::table($this->table)
            ->where('applicant_id', $applicantId)
            ->update(['is_default' => 'n']);

        DB::table($this->table)
            ->where('id', $resumeId)
            ->update(['is_default' => 'y']);
    }

    public function fetch($applicantId, $getDefault = false)
    {
        if ($getDefault) {
            return DB::table($this->table)
                ->where('applicant_id', '=', $applicantId)
                ->where('is_default', '=', 'y')
                ->get();
        } else {
            return DB::table($this->table)
                ->where('applicant_id', '=', $applicantId)
                ->get();
        }
    }

    public function deleteResume($id)
    {
        DB::table('users')
            ->where('id', '=', $id)
            ->delete();
    }

}
