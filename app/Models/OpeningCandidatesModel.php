<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OpeningCandidatesModel extends Model
{
    protected $table = 'opening_candidates';

    protected $alias = ' AS oc ';

    protected $fillable = ['opening_id', 'candidate_id', 'status'];

    public function fetch($applicationId)
    {
    	return DB::table($this->table . $this->alias)
            ->join('openings AS o', 'o.id', '=', 'oc.opening_id')
    		->join('applicants AS a', 'a.id', '=', 'oc.candidate_id')
            ->select('o.*', 'oc.*', 'oc.status AS application_status', 'a.*')
    		->where('oc.id', '=', $applicationId)
    		->get();
    }

    public function fetchAll()
    {
        return DB::table($this->table . $this->alias)
            ->select(
                'oc.id',
                'oc.opening_id',
                'oc.candidate_id',
                'o.title',
                'oc.created_at AS date_applied',
                'oc.status',
                DB::raw('CONCAT(a.first_name, " " , a.last_name) AS applicant_name')
            )
            ->join('applicants as a', 'a.id', '=', 'oc.candidate_id')
            ->join('openings as o', 'o.id', '=', 'oc.opening_id')
            ->where('oc.status', '!=', 'h')
            ->get();
    }

    public function moveStep($candidateId, $openingId, $status)
    {
        return DB::table($this->table)
            ->where('opening_id', '=', $openingId)
            ->where('candidate_id', '=', $candidateId)
            ->update(['status' => $status]);
    }

    public function hasApplication($openingId, $applicantId)
    {
        return DB::table($this->table)
            ->where('opening_id', '=', $openingId)
            ->where('candidate_id', '=', $applicantId)
            ->count();
    }
}