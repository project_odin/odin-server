<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PositionsModel extends Model
{    
	protected $table = 'positions';

    protected $fillable = ['position', 'description'];
    
    public function fetchAll()
    {
        return DB::table($this->table)->select()->get();
    }
}
