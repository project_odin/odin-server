<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LeaveRequestsModel extends Model
{    

    protected $table = 'leave_requests';
    
    protected $fillable = [
        'user_id',
        'leave_type_id',
        'date_start',
        'date_end',
        'reason',
        'employee_remarks',
        'hr_remarks',
        'leave_status'
    ];

    public function getLeaveRequest($user)
    {
        return DB::table($this->table)
            ->join('leave_information as li', 'li.id', '=', 'leave_requests.leave_type_id')
            ->select('li.leave_type', 'leave_requests.*')
            ->where('user_id', '=', $user)
            ->get();
    }

    public function getEmployeeLeaveRequests()
    {
        return DB::table($this->table)
            ->join('employees AS e', 'e.user_id', '=', 'leave_requests.user_id')
            ->join('profiles AS p', 'p.user_id', '=', 'leave_requests.user_id')
            ->join('leave_information AS li', 'li.id', '=', 'leave_requests.leave_type_id')
            ->select(
                'e.user_id',
                'e.employee_number',
                'p.first_name',
                'p.last_name',
                'li.leave_type',
                'leave_requests.id',
                'leave_requests.date_start',
                'leave_requests.date_end',
                'leave_requests.reason',
                'leave_requests.employee_remarks',
                'leave_requests.leave_status',
                'leave_requests.created_at'
            )
            ->get();
    }

    public function getLeave($id)
    {
        return DB::table($this->table)
            ->join('employees AS e', 'e.user_id', '=', 'leave_requests.user_id')
            ->join('profiles AS p', 'p.user_id', '=', 'leave_requests.user_id')
            ->join('leave_information AS li', 'li.id', '=', 'leave_requests.leave_type_id')
            ->select(
                'e.user_id',
                'e.employee_number',
                'p.first_name',
                'p.last_name',
                'li.leave_type',
                'leave_requests.id',
                'leave_requests.date_start',
                'leave_requests.date_end',
                'leave_requests.reason',
                'leave_requests.employee_remarks',
                'leave_requests.leave_status',
                'leave_requests.created_at'
            )
            ->where('leave_requests.id', '=', $id)
            ->get();
    }

    public function leave_information()
    {
        return $this->hasOne('App\Models\LeaveInformationModel', 'id', 'leave_type_id');
    }

    public function updateLeaveRequest($data, $id)
    {
        $request                   = $this->findOrFail($id);
        $request->leave_type_id    = $data['leave_type_id']; 
        $request->date_start       = $data['date_start']; 
        $request->date_end         = $data['date_end']; 
        $request->reason           = $data['reason']; 
        $request->employee_remarks = $data['employee_remarks']; 
        $request->save();
    }

    public function disapproveLeave($id)
    {
        $request               = $this->findOrFail($id);
        $request->hr_remarks   = $data['hr_remarks']; 
        $request->leave_status = 'd'; 
        $request->save();
    }

    public function approveLeave($id)
    {
        $request               = $this->findOrFail($id);
        $request->hr_remarks   = $data['hr_remarks']; 
        $request->leave_status = 'a'; 
        $request->save();
    }

    public function hasApprovedLeaveRequest($employeeNumber, $date)
    {

        $dateFiled = date('Y-m-d', strtotime($date));
        return DB::table($this->table)
            ->join('employees as e', 'e.user_id', '=', 'leave_requests.user_id')
            ->select('leave_requests.*')
            ->where('employee_number', '=', $employeeNumber)
            ->where('leave_requests.date_start', '>=', $dateFiled)
            ->where('leave_requests.date_end', '>=', $dateFiled)
            ->where('leave_requests.leave_status', '=', 'a')
            ->count();
    }
}
