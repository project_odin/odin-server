<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class OvertimeRequestsModel extends Model
{
    protected $table = 'employee_overtime_requests';

    protected $fillable = [
        'id',
        'employee_number',
        'overtime_date',
        'overtime_from',
        'overtime_to',
        'total_hours',
        'status',
        'remarks',
        'created_at',
        'updated_at'
    ];

    public function hasApprovedOvertimeRequest($employeeNumber, $date)
    {

        $dateFiled = date('Y-m-d', strtotime($date));
        return DB::table($this->table)
            ->where('employee_number', '=', $employeeNumber)
            ->where('overtime_date', '=', $dateFiled)
            ->where('status', '=', 'a')
            ->count();
    }
}
