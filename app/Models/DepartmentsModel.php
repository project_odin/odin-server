<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DepartmentsModel extends Model
{
    protected $table = 'departments';

    protected $fillable = ['department'];

    public function fetchAll()
    {
        return DB::table($this->table)->select()->get();
    }
}
