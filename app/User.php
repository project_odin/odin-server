<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'password', 'client_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Check if there are any existing username in users table
     * 
     * @param  String
     * @return array
     */
    public function checkExistingUsername($username)
    {
        $users = DB::table('users')
            ->where('username', $username)
            ->count();

        return $users;
    }

    public function profiles()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function employees()
    {
        return $this->hasOne('App\Models\EmployeeModel', 'employee_number', 'username');
    }

    public function positions()
    {
        return $this->hasOne('App\Models\PositionsModel', 'id', 'position_id');
    }

    public function departments()
    {
        return $this->hasOne('App\Models\DepartmentsModel', 'id', 'department_id');
    }
}
