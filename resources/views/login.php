<!doctype html>
<html>
    <head>
        <title>Application</title>
        <?php include_once('includes/styles.php'); ?>
        <link rel="stylesheet" href="assets/styles/apply.css">
    </head>
    <body id='home' ng-controller="LoginController as login">
    <div class="pusher">
       <div class="ui grid">
    <div class="ui stackable two column centered grid">
        <div class="column center aligned">
            <h1 class="light">Asia Wide</h1>
        </div>
        <div class="column center aligned">
            <h1 class="thin">Human Resources and Consultancy Corp. </h1>
            <h3 class="thin">Something awesome awaits! </h3>
        </div>
        <div class="three column centered row">
            <div class="column">
                <div class="ui error message" ng-show="login.showMessage">
                    {{login.message}}
                </div>
                <div class="ui raised form segment form-segment">
                    <form ng-submit="login.login()" name="loginForm">
                        <div class="ui form">
                            <div class="field">
                                <label>Username</label>
                                <div class="ui left fluid icon input">
                                    <input type="text" ng-model='login.username' required>
                                    <i class="user icon"></i>
                                </div>
                            </div>
                            <div class="field">
                                <label>Password</label>
                                <div class="ui left fluid icon input">
                                    <input type="password" ng-model="login.password" required>
                                    <i class="lock icon"></i>
                                </div>
                            </div>
                            <div class="field">
                                <button type="submit" ng-disabled="!loginForm.$valid" class="fluid ui blue button" ng-class="{loading : login.loading}">Sign in</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ui center aligned basic segment">
                    <a href="/apply" style="">Create an account!</a>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
    <!-- Application Dependencies -->
    <?php include_once('includes/scripts.php'); ?>

    </body>
</html>
