<!doctype html>
<!doctype html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Application</title>
        <?php include_once('includes/styles.php'); ?>
        <link rel="stylesheet" href="assets/styles/apply.css">
    </head>
    <body id='home' ng-controller="ApplicationController as application">
    <div class="pusher">
        <div class="ui container">
            <div class="ui two column doubling grid container padded-top">
                <br>
            </div>
                <div class="ui two column doubling grid container">
                    <div class="column registration-title">
                        <h1 class="semi-light">Sign up to Asia Wide </h1>
                        <h3 class="light">You can get the most of your learning habit! </h3>
                        <p class="thin small-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis autem ut illo, consequatur enim aut iusto quidem in exercitationem error.</p>
                        Already have an account? <a href="/login" style="">Sign in</a> here!
                    </div>
                    <div class="column">
                        <div class="ui raised form segment form-segment">
                        <form name="applicationForm" class="myForm" ng-submit="application.register()" data-rules='application.validationRules' novalidate validate-form>
                            <div class="ui form">
                                <div class="field">
                                    <label>Firstname</label>
                                    <div class="ui left fluid icon input">
                                        <input type="text" ng-model="application.user.first_name" name="firstname">
                                        <i class="child icon"></i>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Lastname</label>
                                    <div class="ui left fluid icon input">
                                        <input type="text" ng-model="application.user.last_name" name="lastname">
                                        <i class="users icon"></i>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Email Address</label>
                                    <div class="ui left fluid icon input">
                                        <input type="email" ng-model="application.user.email" name="email">
                                        <i class="mail icon"></i>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Username</label>
                                    <div class="ui left fluid icon input">
                                        <input type="text" ajax-validate name="username" ng-model="application.user.username" required data-content="Add users to your feed">
                                        <i class="at icon"></i>
                                    </div>
                                </div>
                                <div class="field">
                                    <label>Password</label>
                                    <div class="ui left fluid icon input">
                                        <input type="password" ng-model="application.user.password" name="password" required ng-minlength='8'>
                                        <i class="lock icon"></i>
                                    </div>
                                </div>
                                <div class="field">
                                    <button type="submit" class="fluid ui blue button" ng-class="{loading : application.loading}" ng-disabled="!applicationForm.$valid">Sign in </button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- Application Dependencies -->
    <?php include_once('includes/scripts.php'); ?>
    </body>
</html>
