
<h4>Asiawide Recruitment Password Reset Request</h4>

<p>You have requested to reset your password.</p>
<br>
<p>You can now login to our portal (http://app.mis-client.com/) using these credentials</p>
<br>
<p>Username: <?php echo $username?></p>
<p>Password: <?php echo $newpassword?></p>
<br>
<p>Thank you and God Bless</p>
<b>Note:</b>
<p>Please change your password once logged in.</p>
<p>Asia Wide Recruitment and Consulting Corp.</p>
<?php
