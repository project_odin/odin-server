<h1>Candidates List</h1>

<p>As of <?php echo date('Y-m-d'); ?></p>
<table cellspacing='0' cellpadding="3" border='1' style="width: 100%">
	<thead>
		<tr>
        <th st-sort="title">Applicant ID</th>
        <th st-sort="company">Applicant Name</th>
        <th st-sort="created_at">Opening Applied</th>
        <th st-sort="opening_type">Date Applied</th>
        <th st-sort="opening_type">Status</th>
		</tr>
	</thead>
	<tbody>
		 <?php foreach ($data as $row) { ?>
    		<tr>
        		<td><?php echo $row->candidate_id; ?></td>
        		<td><?php echo $row->applicant_name; ?></td>
        		<td><?php echo $row->title; ?></td>
        		<td><?php echo date('Y-m-d', strtotime($row->date_applied)); ?></td>
        		<td><?php echo getStatus($row->status) ?></td>
        	</tr>
        <?php } ?>
	</tbody>
</table>

<?php 

function getStatus($status) {
	switch($status) {
		case 's' : return 'For screening'; break;
		case 'p' : return 'For phone interview'; break;
		case 'f' : return 'For face to face interview'; break;
		case 'j' : return 'For job offer'; break;
	}
}