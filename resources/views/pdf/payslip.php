<?php

    $gross = 0;
    $deduction = 0;
    $net = 0;

?>

<table style="width: 100%">
    <tr>
        <th colspan='5' style="font-size: 20px">A&P Service Incorporated</th>
    </tr>    
    <tr>
        <th colspan='5' style="font-size: 13px">Payslip</th>
    </tr>
    <tr>
        <th colspan='5' style="font-size: 13px">For payroll period : <?php echo date('F d, Y', strtotime($data[0]->payroll_from)) . ' - ' . date('F d, Y', strtotime($data[0]->payroll_from))?></th>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr style="font-size: 12px;">
        <td>Employee Number</td>
        <td><?php echo $data[0]->employee_number; ?></td>
        <td>Employee Name</td>
        <td><?php echo $data[0]->first_name . ' ' . $data[0]->last_name; ?></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td colspan='2' style="text-align: center; font-size: 11">
            Earnings
        </td>
        <td></td>
        <td colspan='2' style="text-align: center; font-size: 11"> 
            Deductions
        </td>
    </tr>
    <tr style="font-size: 12px">
        <td>Rate per hour</td>
        <td style="text-align: right"><?php echo $data[0]->rate_per_hour;?></td>
        <td></td>
        <td>SSS</td>
        <td style="text-align: right"><?php echo $data[0]->sss?></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Hours Rendered</td>
        <td style="text-align: right"><?php echo $data[0]->hours?></td>
        <td></td>
        <td>Philhealth</td>
        <td style="text-align: right"><?php echo $data[0]->philhealth?></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Basic Pay</td>
        <td style="text-align: right"><?php echo $data[0]->total_basic?></td>
        <td></td>
        <td>HDMF</td>
        <td style="text-align: right"><?php echo $data[0]->hdmf?></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Overtime Pay</td>
        <td style="text-align: right"><?php echo $data[0]->overtime_pay?></td>
        <td></td>
        <td>Withholding Tax</td>
        <td style="text-align: right"><?php echo $data[0]->tax?></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Spacial Holiday Pay</td>
        <td style="text-align: right"><?php echo $data[0]->special_holiday_pay?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Regular Holiday Pay</td>
        <td style="text-align: right"><?php echo $data[0]->legal_holiday_pay?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="font-size: 12px">
        <td>ECOLA</td>
        <td style="text-align: right"><?php echo $data[0]->ecola?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Night Diffirential Pay</td>
        <td style="text-align: right"><?php echo $data[0]->night_diff_pay?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="font-size: 12px">
        <td><br></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="font-size: 12px">
        <td>Gross Pay</td>
        <td style="text-align: right"><?php 
            $gross += $data[0]->total_basic;
            $gross += $data[0]->overtime_pay;
            $gross += $data[0]->special_holiday_pay;
            $gross += $data[0]->legal_holiday_pay;
            $gross += $data[0]->ecola;
            $gross += $data[0]->night_diff_pay;

            echo $gross;

        ?></td>
        <td></td>
        <td>Total Deduction</td>
        <td style="text-align: right"><?php 
            $deduction += $data[0]->sss;
            $deduction += $data[0]->philhealth;
            $deduction += $data[0]->hdmf;
            $deduction += $data[0]->tax;

            echo $deduction;
        ?></td>
    </tr>
    <tr style="font-size: 12px">
        <td colspan="2"><strong>Net Pay</strong></td>
        <td></td>
        <td colspan="2" style="text-align: right"><strong><?php echo $gross - $deduction?></strong></td>
    </tr>
</table>

