<h2 style="margin-top: 0px; margin-bottom: 0px;">Audit Trail</h2>
<p>As of <?php echo date('Y-m-d'); ?></p>
<table cellspacing='0' cellpadding="3" border='1' style="width: 100%">
	<thead>
		<tr>
        <th st-sort="title">ID</th>
        <th st-sort="company">User ID</th>
        <th st-sort="created_at">User</th>
        <th st-sort="opening_type">Action Taken</th>
        <th st-sort="client_id">Date</th>
		</tr>
	</thead>
	<tbody>
		 <?php foreach ($data as $row) { ?>
    		<tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->user_id; ?></td>
                <td><?php echo $row->first_name . ' ' . $row->middle_name . ' ' . $row->last_name; ?></td>
                <td><?php echo $row->action_taken; ?></td>
        		<td><?php echo $row->created_at; ?></td>

        	</tr>
        <?php } ?>
	</tbody>
</table>