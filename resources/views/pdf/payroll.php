<style>
    body {
        font-size : 11px;
    }
</style>
<body>
    <h1>Payroll Summary</h1>
    <table cellspacing='0' cellpadding="3" border='1' style="width: 100%">
        <thead>
            <tr>
            <th st-sort="title">Applicant ID</th>
            <th st-sort="company">Applicant Name</th>
            <th st-sort="created_at">Payroll Period</th>
            <th st-sort="opening_type">Total Earning (PHP) </th>
            <th st-sort="opening_type">Total Deduction (PHP) </th>
            <th st-sort="opening_type">Net Salary (PHP) </th>
            </tr>
        </thead>
        <tbody>
             <?php foreach ($data as $row) { ?>
                <tr>
                    <td st-sort="title"><?php echo $row->employee_number; ?></td>
                    <td style="text-align: left"><?php echo $row->first_name . ' '  . $row->last_name; ?></td>
                    <td st-sort="created_at"><?php echo date('Y-m-d', strtotime($row->payroll_from)) . ' - '  . date('Y-m-d', strtotime($row->payroll_to)); ?></td>
                    <td style="text-align: right"><?php echo number_format($row->total_earning, 2) ?></td>
                    <td style="text-align: right"><?php echo number_format($row->total_deduction, 2) ?></td>
                    <td style="text-align: right"><?php echo number_format(($row->total_earning - $row->total_deduction), 2) ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>