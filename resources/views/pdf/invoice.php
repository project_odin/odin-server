

<h2 style="margin-top: 0px; margin-bottom: 0px;">Job Openings</h2>
<p>As of <?php echo date('Y-m-d'); ?></p>
<table cellspacing='0' cellpadding="3" border='1' style="width: 100%">
	<thead>
		<tr>
        <th st-sort="title">Title</th>
        <th st-sort="company">Company Name</th>
        <th st-sort="created_at">Date Opened</th>
        <th st-sort="opening_type">Opening Type</th>
        <th st-sort="client_id">Candidates</th>
		</tr>
	</thead>
	<tbody>
		 <?php foreach ($data as $row) { ?>
    		<tr>
        		<td><?php echo $row->title; ?></td>
        		<td><?php echo $row->company; ?></td>
        		<td><?php echo date('Y-m-d', strtotime($row->created_at)); ?></td>
        		<td><?php echo ($row->opening_type === 'pt') ? 'Part Time' : 'Full Time'; ?></td>
        		<td><?php echo $row->candidates; ?></td>
        	</tr>
        <?php } ?>
	</tbody>
</table>