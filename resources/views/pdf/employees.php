<h2 style="margin-top: 0px; margin-bottom: 0px;">A&P Service Incorporated</h2>
<h2 style="margin-top: 0px; margin-bottom: 0px;">Employee List</h2>

<table cellspacing='0' cellpadding="3" border='1' style="width: 100%; margin-top: 50px;">
    <thead>
        <tr>
        <th st-sort="title">Employee Number</th>
        <th st-sort="company">Employee Name</th>
        <th st-sort="created_at">Department</th>
        <th st-sort="opening_type">Position</th>
        <th st-sort="client_id">Date Hired</th>
        </tr>
    </thead>
    <tbody>
         <?php foreach ($data as $row) { ?>
            <tr>
                <td><?php echo $row->employee_id; ?></td>
                <td><?php echo $row->first_name . ' ' . $row->middle_name . ' ' . $row->last_name; ?></td>
                <td><?php echo $row->department; ?></td>
                <td><?php echo $row->position; ?></td>
                <td><?php echo date('Y-m-d', strtotime($row->hire_date)); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>