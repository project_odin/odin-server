
<!doctype html>
<html>
    <head>
        <title>Recruitment</title>
<?php include_once('includes/styles.php'); ?>
        <link rel="stylesheet" href="assets/styles/index.css">
    </head>
    <body>
       <div class="pusher">
    <div class="full height">
        <div class="container">
            <div class="ui grid">
                
                <ng-include  
                	class="ui fixed inverted blue  main menu" 
                	src="'scripts/common/views/applicant-navigation.html'">
                </ng-include>

                <ng-include 
                	ng-controller="SidebarController as sidebar" 
                	src="'scripts/common/views/sidebar.html'" 
                	class="four wide column" id="left-panel">
                </ng-include>

                <div class="twelve wide column" id="app-content">
                </div>
            </div>
        </div>
    </div>
</div>

    </body>

    <!-- Application Dependencies -->
    <?php include_once('includes/scripts.php'); ?>

</html>
