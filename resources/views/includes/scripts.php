	<script src="bower_components/jquery/dist/jquery.js"></script>
	<script src="bower_components/angular/angular.js"></script>
	<script src="bower_components/angular-resource/angular-resource.js"></script>
	<script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/satellizer/satellizer.js"></script>
	<script src="bower_components/semantic-ui/dist/semantic.js"></script>
	
    <script src="scripts/app.js"></script>

    <script src="scripts/applicant/module.js"></script>
    <script src="scripts/applicant/controller.js"></script>
    <script src="scripts/applicant/service.js"></script>

    <script src="scripts/login/module.js"></script>
    <script src="scripts/login/controller.js"></script>
    <script src="scripts/login/service.js"></script>

    <script src="scripts/dashboard/module.js"></script>
    <script src="scripts/dashboard/controller.js"></script>
    <script src="scripts/dashboard/service.js"></script>

    <script src="scripts/common/module.js"></script>
    <script src="scripts/common/directives.js"></script>
    <script src="scripts/common/service.js"></script>