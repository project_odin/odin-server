<?php

use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('applicants')->delete();
        
		\DB::table('applicants')->insert(array (
			0 => 
			array (
				'id' => '7',
				'first_name' => 'qwe',
				'last_name' => 'qwe',
				'email' => 'qwe@qwe.qwe',
				'status' => 'n',
				'created_at' => '2015-08-04 16:40:12',
				'updated_at' => '2015-08-04 16:40:12',
			),
			1 => 
			array (
				'id' => '8',
				'first_name' => 'asd',
				'last_name' => 'asd',
				'email' => 'asd1@asda.asda',
				'status' => 'n',
				'created_at' => '2015-08-04 16:41:51',
				'updated_at' => '2015-08-04 16:41:51',
			),
			2 => 
			array (
				'id' => '15',
				'first_name' => 'Kevin',
				'last_name' => 'Padilla',
				'email' => 'kevin.padilla0717@gmail.com',
				'status' => 'n',
				'created_at' => '2015-08-12 12:41:04',
				'updated_at' => '2015-08-12 12:41:04',
			),
		));
	}

}
