<?php

use Illuminate\Database\Seeder;

class ResumeTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('resume')->delete();
        
		\DB::table('resume')->insert(array (
			0 => 
			array (
				'id' => '11',
				'applicant_id' => '15',
				'title' => 'TEst',
				'file' => 'PADILLA_20150812124131.pdf',
				'is_default' => 'y',
				'created_at' => '2015-08-12 12:41:31',
				'updated_at' => '2015-08-12 12:41:31',
			),
		));
	}

}
