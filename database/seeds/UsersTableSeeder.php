<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users')->delete();
        
		\DB::table('users')->insert(array (
			0 => 
			array (
				'id' => '1',
				'username' => 'admin',
				'password' => '$2y$10$wJXBeKgQITqVzKm1NUN.cuTMH8gheANzOQGH/JkVNdPMP2NDXvb/S',
				'client_id' => 'admin',
				'remember_token' => NULL,
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '0000-00-00 00:00:00',
			),
			1 => 
			array (
				'id' => '3',
				'username' => 'tester@yahoo.com',
				'password' => '$2y$10$GoqKSz4dcZcre6sxTLFEG.DwcS3gpua4m/1dp0cdMX9V.9d2AgCZS',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-03 15:53:27',
				'updated_at' => '2015-08-03 15:53:27',
			),
			2 => 
			array (
				'id' => '4',
				'username' => 'testhr',
				'password' => '$2y$10$6NEYi9vL22jxOWNYgDNZ1u75x1R8DBc9MdUy/NW2LH4E5mWvTMpMy',
				'client_id' => 'hr',
				'remember_token' => NULL,
				'created_at' => '2015-08-04 16:23:54',
				'updated_at' => '2015-08-04 16:23:54',
			),
			3 => 
			array (
				'id' => '5',
				'username' => 'TEstdb@yahoo.com',
				'password' => '$2y$10$8Ol6fd1GCIHRY1Or/xWdqe37MyJbvtBHXrpruWuS4ZLxYQOjXobay',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-04 16:33:21',
				'updated_at' => '2015-08-04 16:33:21',
			),
			4 => 
			array (
				'id' => '6',
				'username' => 'TEstdb22@yahoo.com',
				'password' => '$2y$10$EnCkkewwewds8ukhJgD9n.hTPM9lmaqBvMvJ8VBaEZkfzatA1Pcti',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-04 16:34:49',
				'updated_at' => '2015-08-04 16:34:49',
			),
			5 => 
			array (
				'id' => '7',
				'username' => 'qwe@qwe.qwe',
				'password' => '$2y$10$olbC7rq/t7iB2WsCWaKvvOciF..1VVtgq.On364wwjipewVvCCEWG',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-04 16:40:12',
				'updated_at' => '2015-08-04 16:40:12',
			),
			6 => 
			array (
				'id' => '8',
				'username' => 'asd1@asda.asda',
				'password' => '$2y$10$rew6/d.3O/zQ2rKMBj.dKORFv1XCsoSrfOd6u2hQ9vcXSPVtr50Uy',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-04 16:41:51',
				'updated_at' => '2015-08-04 16:41:51',
			),
			7 => 
			array (
				'id' => '15',
				'username' => 'kevin.padilla0717@gmail.com',
				'password' => '$2y$10$CTBROrf4qH3swTFTIJ.KZeDfBlFUFQoHlyXTMu8CnR2WkS2QkhRU6',
				'client_id' => 'applicant',
				'remember_token' => NULL,
				'created_at' => '2015-08-12 12:41:04',
				'updated_at' => '2015-08-12 12:41:04',
			),
		));
	}

}
