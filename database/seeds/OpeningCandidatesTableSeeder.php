<?php

use Illuminate\Database\Seeder;

class OpeningCandidatesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('opening_candidates')->delete();
        
	}

}
