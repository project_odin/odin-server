<?php

use Illuminate\Database\Seeder;

class OpeningsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('openings')->delete();
        
	}

}
