<?php

use Illuminate\Database\Seeder;

class StepsAssigneeTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('steps_assignee')->delete();
        
	}

}
