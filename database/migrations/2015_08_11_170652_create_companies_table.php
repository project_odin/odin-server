<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {       
        Schema::dropIfExists('companies');   

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code')->unique();
            $table->string('name', 50);
            $table->string('location',255);
            $table->string('contact_person', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
