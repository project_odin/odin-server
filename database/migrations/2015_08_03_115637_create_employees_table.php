<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('employees');

        Schema::create('employees', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->integer('user_id');    
            $table->integer('branch_id');    
            $table->integer('excemption');    
            $table->string('employee_number', 11);
            $table->string('contact_number', 11);
            $table->string('civil_status', 20);
            $table->char('gender', 1);
            $table->string('address', 255);
            $table->date('birth_date');
            $table->integer('department_id');
            $table->integer('position_id');
            $table->date('hire_date');
            $table->date('endo_date');
            $table->string('account_number', 20);
            $table->string('phil_health_number', 20);
            $table->string('sss_number', 20);
            $table->string('tin_number', 20);
            $table->string('pagibig_number', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
