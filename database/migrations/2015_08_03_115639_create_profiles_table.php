<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('profiles');
        
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('first_name', 50);
            $table->string('middle_name', 50);
            $table->string('last_name', 50);
            $table->string('address', 100);
            $table->string('birth_place', 50);
            $table->date('birthdate');
            $table->string('cellphone', 11);
            $table->string('citizensip', 20);
            $table->string('email', 50);
            $table->string('field', 50);
            $table->string('nick', 20);
            $table->string('religion');
            $table->string('school');
            $table->string('telephone', 8);
            $table->string('year_graduated', 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
