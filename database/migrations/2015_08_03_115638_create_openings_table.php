<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('openings');
        
        Schema::create('openings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->string('opening_type', 50);
            $table->string('location', 50);
            $table->text('job_description', 100);
            $table->integer('company_id');
            $table->string('status', '50');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
