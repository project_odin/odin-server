<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('leave_requests');   

        Schema::create('leave_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('leave_type_id', 1);
            $table->date('date_start');
            $table->date('date_end');
            $table->string('reason', 255);
            $table->string('employee_remarks', 255);
            $table->string('hr_remarks', 255);
            $table->string('leave_status', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
