<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('resume');

        Schema::create('resume', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('applicant_id');
            $table->string('title', 50);
            $table->text('file');
            $table->text('is_default', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
